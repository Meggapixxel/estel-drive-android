package com.zhydenko.vadim.esteldrive.broadcast;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.zhydenko.vadim.esteldrive.models.User;

public class BootReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        AlarmReceiver alarm = new AlarmReceiver(context);
        if (intent.getAction().equals("android.intent.action.BOOT_COMPLETED")) {
            alarm.setAlarm(User.restoreScheduledHours(context), User.restoreScheduledMinutes(context));
        }
    }

}