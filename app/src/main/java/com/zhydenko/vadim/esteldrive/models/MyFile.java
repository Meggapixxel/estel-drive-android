package com.zhydenko.vadim.esteldrive.models;

import java.util.Date;

public class MyFile {

    private final String name, dropboxFileId, dropboxToken;
    private String pathDisplay;
    private final Date changeDate;
    private final Long realSize;
    
    public MyFile(String name, String pathDisplay, Date changeDate, Long realSize) {
        this.name = name;
        this.pathDisplay = pathDisplay;
        this.changeDate = changeDate;
        this.realSize = realSize;
        this.dropboxFileId = null;
        this.dropboxToken = null;
    }

    public MyFile(String name, String pathDisplay, Date changeDate, Long realSize, String dropboxFileId, String dropboxToken) {
        this.name = name;
        this.pathDisplay = pathDisplay;
        this.changeDate = changeDate;
        this.realSize = realSize;
        this.dropboxFileId = dropboxFileId;
        this.dropboxToken = dropboxToken;
    }

    public boolean isDir() {
        return changeDate == null && realSize == null;
    }

    public String getDropboxFileId() {
        return dropboxFileId;
    }

    public String getName() {
        return name;
    }

    public String getPathDisplay() {
        return pathDisplay;
    }

    public void setPathDisplay(String pathDisplay) {
        this.pathDisplay = pathDisplay;
    }

    public Date getChangeDate() {
        return changeDate;
    }

    public Long getRealSize() {
        return realSize;
    }

    public String getDropboxToken() {
        return dropboxToken;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof MyFile) {
            MyFile myFile = (MyFile) obj;
            if (name != null) return myFile.getPathDisplay().equalsIgnoreCase(pathDisplay) && myFile.getName().equalsIgnoreCase(name);
            else return myFile.getPathDisplay().equalsIgnoreCase(pathDisplay);
        }
        return false;
    }
}
