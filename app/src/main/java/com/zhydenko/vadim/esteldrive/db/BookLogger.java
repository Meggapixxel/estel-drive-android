package com.zhydenko.vadim.esteldrive.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.zhydenko.vadim.esteldrive.fragments.LoggerFragment;
import com.zhydenko.vadim.esteldrive.models.LoggerItem;
import com.zhydenko.vadim.esteldrive.models.MyFile;
import com.zhydenko.vadim.esteldrive.utils.Utils;

import java.util.ArrayList;
import java.util.Date;

public class BookLogger extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 2;
    private static final String DATABASE_NAME = "DownloadManager.db";
    private static final String TABLE_DOWNLOADS = "Downloads";
    private static final String KEY_ID = "id";
    private static final String KEY_NAME = "name";
    private static final String KEY_STATUS = "status";
    private static final String KEY_DATE = "date";
    private static final String KEY_PATH = "path";

    public BookLogger(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_DOWNLOADS_TABLE = "CREATE TABLE " + TABLE_DOWNLOADS + "("
                + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + KEY_NAME + " TEXT, "
                + KEY_STATUS + " INTEGER, "
                + KEY_DATE + " TEXT, "
                + KEY_PATH + " TEXT );";
        db.execSQL(CREATE_DOWNLOADS_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_DOWNLOADS);
        onCreate(db);
    }

    public void addDownload(String name, boolean status, long date, String path) {
        ContentValues values = new ContentValues();
        values.put(KEY_NAME, name);
        values.put(KEY_STATUS, status ? 0 : 1);
        values.put(KEY_DATE, date);
        values.put(KEY_PATH, path);
        SQLiteDatabase db = getWritableDatabase();
        db.insert(TABLE_DOWNLOADS, null, values);
        db.close();
    }

    public ArrayList<LoggerItem> databaseToString() {
        SQLiteDatabase db = getWritableDatabase();

        db.execSQL("DELETE FROM " + TABLE_DOWNLOADS + " WHERE " + KEY_DATE + " <= " + Utils.getDateStringMinus(14));

        Cursor c = db.rawQuery("SELECT * FROM " + TABLE_DOWNLOADS, null);
        c.moveToFirst();

        ArrayList<LoggerItem> arrayList = new ArrayList<>();

        while (!c.isAfterLast()) {
            LoggerItem loggerItem = cursorToContent(c);
            arrayList.add(loggerItem);
            c.moveToNext();
        }
        db.close();
        c.close();
        return arrayList;
    }

    private LoggerItem cursorToContent(Cursor c) {
        return new LoggerItem(c.getString(c.getColumnIndex(KEY_NAME)),
                c.getString(c.getColumnIndex(KEY_STATUS)),
                c.getLong(c.getColumnIndex(KEY_DATE)),
                c.getString(c.getColumnIndex(KEY_PATH)));
    }
}
