package com.zhydenko.vadim.esteldrive.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.dropbox.core.DbxRequestConfig;
import com.dropbox.core.v2.DbxClientV2;
import com.dropbox.core.v2.files.FileMetadata;
import com.dropbox.core.v2.files.Metadata;
import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializer;
import com.zhydenko.vadim.esteldrive.R;
import com.zhydenko.vadim.esteldrive.models.Department;
import com.zhydenko.vadim.esteldrive.models.User;

import org.json.JSONException;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

public class Network {

    public static boolean isDeviceOnline(Context context) {
        ConnectivityManager connMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        if (networkInfo != null) {
            boolean isConnected = networkInfo.isConnectedOrConnecting();
            boolean isWiFi = networkInfo.getType() == ConnectivityManager.TYPE_WIFI;
            if (User.isWiFiCelluar(context)) {
                boolean isMobile = networkInfo.getType() == ConnectivityManager.TYPE_MOBILE;
                return isConnected | isWiFi | isMobile;

            } else return isConnected | isWiFi;
        }
        return false;
    }

    public static void downloadConfig(Context context, DisposableSingleObserver<List<Department>> configObserver) {
        Single<List<Department>> filesObservable = Single.create(emitter -> {
            try {
                DbxRequestConfig requestConfig = new DbxRequestConfig("dropbox/estelfiles");
                DbxClientV2 mDbxClient = new DbxClientV2(requestConfig, context.getString(R.string.app_token));
                File file = new File(context.getCacheDir(), "config.json");
                List<Metadata> files = mDbxClient.files().listFolder("").getEntries();
                for (Metadata metadata : files) {
                    if (metadata.getName().equals("config.json")) {
                        OutputStream outputStream = new FileOutputStream(file);
                        mDbxClient.files().download(metadata.getPathLower(), ((FileMetadata) metadata).getRev()).download(outputStream);
                        break;
                    }
                }
                emitter.onSuccess(parseFile(readFile(file)));
            } catch (Exception e) {
                emitter.onError(e);
            }
        });
        filesObservable.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribeWith(configObserver);
    }

    public static void checkUserInGroup(String group, DisposableSingleObserver<Boolean> statusObserver) {
        Single<Boolean> filesObservable = Single.create(emitter -> new GraphRequest(
                AccessToken.getCurrentAccessToken(),
                String.format("/%s/members?limit=400&offset=0&fields=id", group),
                null,
                HttpMethod.GET,
                response -> {
                    try { emitter.onSuccess(processCheck(response)); }
                    catch (JSONException e) { emitter.onError(e); }
                }
        ).executeAsync());
        filesObservable.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribeWith(statusObserver);
    }

    public static List<Single<Boolean>> checkInGroups(List<Department> deps) {
        List<Single<Boolean>> list = new ArrayList<>();
        for (Department dep : deps) {
            list.add(check(dep.fbGroupId));
        }
        return list;
    }

    private static Single<Boolean> check(String group) {
        return Single.create(emitter -> new GraphRequest(
                AccessToken.getCurrentAccessToken(),
                String.format("/%s/members?limit=400&offset=0&fields=id", group),
                null,
                HttpMethod.GET,
                response -> {
                    try { emitter.onSuccess(processCheck(response)); }
                    catch (JSONException e) { emitter.onError(e); }
                }
        ).executeAndWait());
    }

    private static String readFile(File file) throws IOException {
        final InputStream inputStream = new FileInputStream(file);
        InputStreamReader inputreader = new InputStreamReader(inputStream);
        BufferedReader bufferedreader = new BufferedReader(inputreader);
        String line;
        StringBuilder stringBuilder = new StringBuilder();
        while (( line = bufferedreader.readLine()) != null) {
            stringBuilder.append(line);
            stringBuilder.append('\n');
        }
        return stringBuilder.toString();
    }

    private static List<Department> parseFile(String fileContent) throws JSONException {
        Gson gson = new Gson();
        Department[] deps = gson.fromJson(fileContent, Department[].class);
        return Arrays.asList(deps);
    }

    private static boolean processCheck(GraphResponse response) throws JSONException {
        String json = response.getJSONObject().getJSONArray("data").toString();
        String userId = AccessToken.getCurrentAccessToken().getUserId();
        if (json != null && userId != null) {
            GsonBuilder gsonBuilder = new GsonBuilder();
            JsonDeserializer<String> deserializer = (json1, typeOfT, context) -> json1.getAsJsonObject().get("id").getAsString();
            gsonBuilder.registerTypeAdapter(String.class, deserializer);
            Gson customGson = gsonBuilder.create();
            List<String> groups = Arrays.asList(customGson.fromJson(json, String[].class));
            return groups.contains(userId);
        }
        return false;
    }
}