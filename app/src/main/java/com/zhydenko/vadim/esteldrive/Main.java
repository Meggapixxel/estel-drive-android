package com.zhydenko.vadim.esteldrive;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.crashlytics.android.Crashlytics;
import com.facebook.AccessToken;
import com.zhydenko.vadim.esteldrive.databinding.ActvityMainBinding;
import com.zhydenko.vadim.esteldrive.fragments.Departments;
import com.zhydenko.vadim.esteldrive.fragments.TabFragment;
import com.zhydenko.vadim.esteldrive.models.User;

import io.fabric.sdk.android.Fabric;

public class Main extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        ActvityMainBinding binding = DataBindingUtil.setContentView(this, R.layout.actvity_main);
        setSupportActionBar(binding.toolbar);
        if (AccessToken.getCurrentAccessToken() == null || !User.isUserSetup(this)) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.containerView, new Departments())
                    .commit();
        } else {
            if (User.isAdditionConfig(this)) {
                getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.containerView, new Departments())
                        .commit();
            } else {
                getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.containerView, new TabFragment())
                        .commit();
            }
        }
    }
}
