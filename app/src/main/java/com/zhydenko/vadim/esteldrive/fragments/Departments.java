package com.zhydenko.vadim.esteldrive.fragments;

import android.Manifest;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginResult;
import com.zhydenko.vadim.esteldrive.R;
import com.zhydenko.vadim.esteldrive.adapters.DepsAdapter;
import com.zhydenko.vadim.esteldrive.broadcast.MainBroadcast;
import com.zhydenko.vadim.esteldrive.custom_view.CircularProgressButton;
import com.zhydenko.vadim.esteldrive.databinding.FragmentDepartmentsBinding;
import com.zhydenko.vadim.esteldrive.models.Department;
import com.zhydenko.vadim.esteldrive.models.User;
import com.zhydenko.vadim.esteldrive.utils.FBAdapterCheck;
import com.zhydenko.vadim.esteldrive.utils.Intro;
import com.zhydenko.vadim.esteldrive.utils.Network;
import com.zhydenko.vadim.esteldrive.utils.Storage;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.annotations.NonNull;
import io.reactivex.observers.DisposableSingleObserver;
import permissions.dispatcher.NeedsPermission;
import permissions.dispatcher.OnPermissionDenied;
import permissions.dispatcher.RuntimePermissions;

@RuntimePermissions
public class Departments extends Fragment implements View.OnClickListener, FBAdapterCheck {

    FragmentDepartmentsBinding binding;
    private CallbackManager callbackManager;
    List<Department> departments = null;
    FacebookCallback<LoginResult> fbCallback = new FacebookCallback<LoginResult>() {
        @Override
        public void onSuccess(LoginResult loginResult) {
            downloadConfig();
        }

        @Override
        public void onCancel() {
            Snackbar.make(binding.getRoot(), R.string.login_cancel, Snackbar.LENGTH_LONG).show();
        }

        @Override
        public void onError(FacebookException exception) {
            Snackbar.make(binding.getRoot(), exception.toString(), Snackbar.LENGTH_LONG).show();
        }
    };

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (AccessToken.getCurrentAccessToken() == null) {
            callbackManager = CallbackManager.Factory.create();
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_departments, container, false);
        binding.bDepsAccept.setOnClickListener(v -> completeSetup());
        if (AccessToken.getCurrentAccessToken() != null) downloadConfig();
        else Intro.show(getActivity(), binding.fbLogin, getContext().getString(R.string.intro_1), getContext().getString(R.string.intro_1_id));
        return binding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        DepartmentsPermissionsDispatcher.onStartWithCheck(this);
    }

    @OnPermissionDenied(Manifest.permission.WRITE_EXTERNAL_STORAGE)
    void showDeniedForPhoneCall() {
        DepartmentsPermissionsDispatcher.onStartWithCheck(this);
    }

    @Override
    @NeedsPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
    public void onStart() {
        super.onStart();
        if (AccessToken.getCurrentAccessToken() == null) {
            binding.fbLogin.setReadPermissions("public_profile");
            binding.fbLogin.setFragment(this);
            binding.fbLogin.registerCallback(callbackManager, fbCallback);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @android.support.annotation.NonNull String[] permissions, @android.support.annotation.NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        DepartmentsPermissionsDispatcher.onRequestPermissionsResult(this, requestCode, grantResults);
    }

    @Override
    public void onClick(View v) {
        if (v instanceof CircularProgressButton && departments != null) {
            CircularProgressButton button = (CircularProgressButton) v;
            Department department = departments.get((int) v.getTag());
            if (!department.selected) {
                depCheck((CircularProgressButton) v, department);
            } else {
                department.selected = false;
                if (!isEnableds()) binding.bDepsAccept.setVisibility(View.GONE);
                processButton(button, false);
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    private void prepareDepartmentsList() {
        ArrayList<Department> myDeps = User.restoreDepartments(getContext());
        DepsAdapter adapter;
        if (myDeps.size() > 0) {
            for (Department dep : departments) {
                dep.selected = myDeps.contains(dep);
            }
            binding.bDepsAccept.setVisibility(View.VISIBLE);
            adapter = new DepsAdapter(departments, this, this);
        } else adapter = new DepsAdapter(departments, this);
        binding.tDepSelection.setText(R.string.select_department);
        binding.rDepsList.setLayoutManager(new LinearLayoutManager(getContext()));
        binding.rDepsList.setAdapter(adapter);
        binding.rDepsList.setVisibility(View.VISIBLE);
    }

    private void processButton(CircularProgressButton button, boolean success) {
        Handler handler = new Handler();
        if (success) {
            handler.postDelayed(button::showComplete, 1000);
            handler.postDelayed(() -> {button.setIdleText(getString(R.string.delete)); button.showIdle();}, 2000);
        } else {
            button.showProgress();
            handler.postDelayed(button::showError, 1000);
            handler.postDelayed(() -> {button.setIdleText(getString(R.string.select)); button.showIdle();}, 2000);
        }
    }

    private void depCheck(CircularProgressButton button, Department department) {
        button.showProgress();
        Network.checkUserInGroup(department.fbGroupId, new DisposableSingleObserver<Boolean>() {
            @Override
            public void onSuccess(@NonNull Boolean status) {
                department.selected = status;
                processButton(button, status);
                if (status) {
                    binding.bDepsAccept.setVisibility(View.VISIBLE);
                    if (!User.isUserSetup(getContext())) Intro.show(getActivity(), binding.bDepsAccept, getString(R.string.intro_2), getString(R.string.intro_2_id));
                } else Snackbar.make(binding.getRoot(), String.format("%s: %s", getString(R.string.not_in_group), department.description), Snackbar.LENGTH_LONG).show();
            }

            @Override
            public void onError(@NonNull Throwable e) {
                Snackbar.make(binding.getRoot(), e.getMessage(), Snackbar.LENGTH_INDEFINITE)
                        .setAction(R.string.try_again, view -> depCheck(button, department))
                        .show();
            }
        });
    }

    private void downloadConfig() {
        binding.fbLogin.setVisibility(View.GONE);
        binding.pDepLoading.setVisibility(View.VISIBLE);
        binding.tDepSelection.setVisibility(View.VISIBLE);
        binding.tDepSelection.setText(R.string.preparing_setup);
        Network.downloadConfig(getContext(), new DisposableSingleObserver<List<Department>>() {
            @Override
            public void onSuccess(@NonNull List<Department> departments) {
                Departments.this.departments = departments;
                prepareDepartmentsList();
                binding.pDepLoading.setVisibility(View.GONE);
            }

            @Override
            public void onError(@NonNull Throwable e) {
                binding.pDepLoading.setVisibility(View.GONE);
                Snackbar.make(binding.getRoot(), e.getMessage(), Snackbar.LENGTH_LONG)
                        .setAction(R.string.try_again, view -> downloadConfig())
                        .show();
            }
        });
    }

    private void completeSetup() {
        if (User.isAdditionConfig(getContext()) && Storage.createDepFolders(departments, getContext())) {
            User.saveDepartments(getContext(), departments);
            getActivity().sendBroadcast(new Intent(MainBroadcast.NOTIFY));
            User.setAdditionConfig(getContext(), false);
            getActivity().getSupportFragmentManager().popBackStack();
            getActivity().getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.containerView, new TabFragment())
                    .commit();
        } else {
            if (Storage.createDepFolders(departments, getContext())) {
                User.saveDepartments(getContext(), departments);
                User.setUserSetup(getContext());
                getActivity().getSupportFragmentManager().popBackStack();
                getActivity().getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.containerView, new TabFragment())
                        .commit();
            } else Snackbar.make(binding.getRoot(), R.string.error_creating_deleting_folder, Snackbar.LENGTH_LONG)
                        .setAction(R.string.try_again, view -> completeSetup())
                        .show();
        }
    }



    private boolean isEnableds() {
        for (Department dep: departments) {
            if (dep.selected) return true;
        }
        return false;
    }

    @Override
    public void needCheck(CircularProgressButton button, Department department) {
        button.showProgress();
        Network.checkUserInGroup(department.fbGroupId, new DisposableSingleObserver<Boolean>() {
            @Override
            public void onSuccess(@NonNull Boolean status) {
                department.selected = status;
                processButton(button, status);
            }

            @Override
            public void onError(@NonNull Throwable e) {
                department.selected = false;
                processButton(button, false);
            }
        });
    }

}

