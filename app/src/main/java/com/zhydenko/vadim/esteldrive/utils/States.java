package com.zhydenko.vadim.esteldrive.utils;

public enum States {

    NEW_DOWNLOAD("0"),
    COUNT_NOTIFY("1"),
    CONTINUE_DOWNLOAD("2");

    private final String text;

    States(final String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return text;
    }

}
