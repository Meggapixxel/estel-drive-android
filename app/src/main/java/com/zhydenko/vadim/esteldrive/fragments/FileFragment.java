package com.zhydenko.vadim.esteldrive.fragments;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.zhydenko.vadim.esteldrive.R;
import com.zhydenko.vadim.esteldrive.databinding.FragmentFileBinding;
import com.zhydenko.vadim.esteldrive.databinding.FragmentFileItemBinding;
import com.zhydenko.vadim.esteldrive.models.Content;
import com.zhydenko.vadim.esteldrive.utils.Alert;
import com.zhydenko.vadim.esteldrive.utils.Storage;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;

import permissions.dispatcher.NeedsPermission;
import permissions.dispatcher.OnPermissionDenied;
import permissions.dispatcher.RuntimePermissions;

import static com.zhydenko.vadim.esteldrive.utils.Storage.rootFolder;

@RuntimePermissions
public class FileFragment extends Fragment implements View.OnClickListener {

    FragmentFileBinding binding;
    FileFragmentAdapter adapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_file, container, false);
        return binding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        binding.floatingActionButton.setOnClickListener(this);
        FileFragmentPermissionsDispatcher.prepareAdapterWithCheck(this);
    }

    @NeedsPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
    void prepareAdapter(){
        adapter = new FileFragmentAdapter();
        binding.fileRecycler.setAdapter(adapter);
        binding.fileRecycler.setLayoutManager(new LinearLayoutManager(getContext()));
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        FileFragmentPermissionsDispatcher.onRequestPermissionsResult(this, requestCode, grantResults);
    }

    @OnPermissionDenied(Manifest.permission.WRITE_EXTERNAL_STORAGE)
    void showDeniedForPhoneCall() {
        FileFragmentPermissionsDispatcher.prepareAdapterWithCheck(this);
    }

    @Override
    public void onClick(View v) {
        FileFragmentPermissionsDispatcher.prepareAdapterWithCheck(this);
    }

    class FileFragmentAdapter extends RecyclerView.Adapter<FileFragmentAdapter.ViewHolder> {

        private ArrayList<Content> folders = new ArrayList<>();
        private ArrayList<Content> files = new ArrayList<>();
        private ArrayList<Content> enList = new ArrayList<>();
        private ArrayList<Content> ruList = new ArrayList<>();
        private ArrayList<String> pathList = new ArrayList<>();

        FileFragmentAdapter() {
            getList(rootFolder);
            listProcess();
            pathList.add(rootFolder.getAbsolutePath());
        }

        private void listProcess() {
            ruList = new ArrayList<>();
            enList = new ArrayList<>();
            spitToDiff();

            ruList = sortContent(ruList);
            enList = sortContent(enList);
            folders = new ArrayList<>();
            folders.addAll(ruList);
            folders.addAll(enList);
        }

        private void spitToDiff() {
            for(int i = 0; i < folders.size(); i++){
                String text = folders.get(i).getName();
                boolean containRu = false;
                for(int j = 0; j < text.length(); j++) {
                    if (Character.UnicodeBlock.of(text.charAt(j)).equals(Character.UnicodeBlock.CYRILLIC)) {
                        containRu = true;
                    }
                }
                if (containRu) ruList.add(folders.get(i));
                else enList.add(folders.get(i));
            }
        }

        private ArrayList<Content> sortContent(ArrayList<Content> smth){
            Content content;
            for (int i = 0; i < smth.size(); i++) {
                content = smth.get(i);
                for (int j = 0; j < smth.size(); j++) {
                    if (i == j) continue;
                    int x = (content.getName().toLowerCase()).compareTo(smth.get(j).getName().toLowerCase());
                    if (x < 0) {
                        content = smth.get(j);
                        smth.set(j, smth.get(i));
                        smth.set(i, content);
                    }
                }
            }
            return smth;
        }

        private void getList(File file) {
            folders = new ArrayList<>();
            files = new ArrayList<>();
            if (file.exists()) {
                for (File f : file.listFiles()) {
                    if (f.isDirectory()) {
                        folders.add(new Content(f.getName(), f.getPath(), "",
                                new Date(f.lastModified()), "", true, 0));
                    } else {
                        String length = Storage.formatSize(f.length());
                        files.add(
                                new Content(f.getName(), f.getAbsolutePath(), "",
                                        new Date(f.lastModified()),
                                        length, false, f.length()));
                    }
                }
            }

            listProcess();
            ArrayList<Content> content = folders;
            folders = new ArrayList<>();

            if (!file.getAbsolutePath().equals(rootFolder.getAbsolutePath())) {
                folders.add(new Content("<-- Назад <--", pathList.get(pathList.size() - 2), "", null, "", true, 0));
            }
            folders.addAll(content);
            folders.addAll(files);
        }

        @Override
        public FileFragmentAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_file_item, parent, false));
        }

        @Override
        public void onBindViewHolder(FileFragmentAdapter.ViewHolder holder, int position) {
            holder.bindContent(folders.get(position));
        }

        @Override
        public int getItemCount() {
            return folders.size();
        }

        class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {

            FragmentFileItemBinding binding;

            ViewHolder(View itemView) {
                super(itemView);
                itemView.setOnClickListener(this);
                itemView.setOnLongClickListener(this);
                binding = DataBindingUtil.bind(itemView);
            }

            void bindContent(Content content) {
                binding.setContent(content);
                if (content.isDir()) binding.imageView.setImageResource(R.drawable.folder);
                else binding.imageView.setImageResource(R.drawable.file);
            }

            @Override
            public void onClick(View v) {
                Content content = binding.getContent();
                if (content.isDir()) {
                    if (content.getName().equals("<-- Назад <--")) {
                        getList(new File(pathList.get(pathList.size() - 2)));
                        pathList.remove(pathList.size() - 1);
                    } else {
                        pathList.add(content.getPath());
                        getList(new File(content.getPath()));
                    }
                    notifyDataSetChanged();
                } else Storage.openFile(getContext(), content.getPath());
            }

            @Override
            public boolean onLongClick(View v) {
                Content content = binding.getContent();
                if (!content.isDir()) {
                    DialogInterface.OnClickListener positiveClick = (dialog, which) -> {
                        Storage.deleteFile(content.getPath());
                        dialog.dismiss();
                    };
                    DialogInterface.OnClickListener negativelick = (dialog, which) -> dialog.dismiss();
                    Alert.show(getContext(), R.string.warning, R.string.delete_file, positiveClick, R.string.yes, negativelick, R.string.no);
                }
                return false;
            }
        }
    }

}