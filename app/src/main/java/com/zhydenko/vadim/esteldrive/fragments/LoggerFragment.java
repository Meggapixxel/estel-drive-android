package com.zhydenko.vadim.esteldrive.fragments;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.zhydenko.vadim.esteldrive.R;
import com.zhydenko.vadim.esteldrive.databinding.FragmentLoggerBinding;
import com.zhydenko.vadim.esteldrive.databinding.FragmentLoggerItemBinding;
import com.zhydenko.vadim.esteldrive.db.BookLogger;
import com.zhydenko.vadim.esteldrive.models.LoggerItem;
import com.zhydenko.vadim.esteldrive.utils.Storage;

import java.util.ArrayList;

public class LoggerFragment extends Fragment implements View.OnClickListener {

    private ArrayList<LoggerItem> items = new ArrayList<>();
    BookLogger bookLogger;
    FragmentLoggerBinding binding;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        bookLogger = new BookLogger(getContext());
        items = bookLogger.databaseToString();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_logger, container, false);
        LoggerFragmentAdapter adapter = new LoggerFragmentAdapter(items, this);
        binding.loggerRecycler.setAdapter(adapter);
        binding.loggerRecycler.setLayoutManager(new LinearLayoutManager(getContext()));
        binding.swipeRefreshLayout.setOnRefreshListener(this::update);
        return binding.getRoot();
    }

    private void update() {
        items = bookLogger.databaseToString();
        binding.loggerRecycler.getAdapter().notifyDataSetChanged();
        binding.swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void onClick(View v) {
        int position = (int) v.getTag();
        LoggerItem loggerItem = items.get(position);
        String filePath =loggerItem.getPath();
        Storage.openFile(getContext(), filePath);
    }

    class LoggerFragmentAdapter extends RecyclerView.Adapter<LoggerFragmentAdapter.ViewHolder> {

        private ArrayList<LoggerItem> items;
        private View.OnClickListener onClickListener;

        LoggerFragmentAdapter(ArrayList<LoggerItem> items, View.OnClickListener onClickListener) {
            this.items = items;
            this.onClickListener = onClickListener;
        }

        @Override
        public LoggerFragmentAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new ViewHolder(LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.fragment_logger_item, parent, false));
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            holder.bindContent(items.get(position), onClickListener, position);
        }

        @Override
        public int getItemCount() {
            return items.size();
        }

        class ViewHolder extends RecyclerView.ViewHolder {

            FragmentLoggerItemBinding binding;

            ViewHolder(View itemView) {
                super(itemView);
                binding = DataBindingUtil.bind(itemView);
            }

            void bindContent(LoggerItem loggerItem, View.OnClickListener onClickListener, int tag) {
                binding.setData(loggerItem);
                if (loggerItem.getStatus().equals("0")) {
                    binding.texto2.setOnClickListener(onClickListener);
                    binding.texto2.setTag(tag);
                }
            }
        }
    }
}
