package com.zhydenko.vadim.esteldrive.fragments;

import android.app.Activity;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;

import com.zhydenko.vadim.esteldrive.R;
import com.zhydenko.vadim.esteldrive.adapters.DepsAdapter;
import com.zhydenko.vadim.esteldrive.broadcast.AlarmReceiver;
import com.zhydenko.vadim.esteldrive.broadcast.MainBroadcast;
import com.zhydenko.vadim.esteldrive.databinding.FragmentMainBinding;
import com.zhydenko.vadim.esteldrive.models.Department;
import com.zhydenko.vadim.esteldrive.models.User;
import com.zhydenko.vadim.esteldrive.utils.Alert;
import com.zhydenko.vadim.esteldrive.utils.Network;
import com.zhydenko.vadim.esteldrive.utils.Storage;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

import static com.zhydenko.vadim.esteldrive.models.User.SHARED_PREF_NAME;

public class MainFragment extends Fragment implements View.OnClickListener {

    private FragmentMainBinding binding;
    private User model;
    private AlarmReceiver alarmReceiver;
//    private MainBroadcast mainBroadcast = null; // api 26

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SharedPreferences sharedPref = getContext().getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        model = new User(sharedPref, getVersionNumber());
        alarmReceiver = new AlarmReceiver(getContext());
//        prepareBroadcast(); // api 26
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_main, container, false);
        downloadConfig();
        return binding.getRoot();
    }

    // api 26
//    @Override
//    public void onDestroy() {
//        super.onDestroy();
//        if (mainBroadcast != null) getActivity().unregisterReceiver(mainBroadcast);
//    }

    // api 26
//    private void prepareBroadcast() {
//        mainBroadcast = new MainBroadcast();
//        IntentFilter filter = new IntentFilter();
//        filter.addAction(MainBroadcast.NOTIFY);
//        getActivity().registerReceiver(mainBroadcast, filter);
//    }

    private void downloadConfig() {
        if (!model.isCheckConfig()) {
            binding.loadIndicator.setVisibility(View.VISIBLE);
            DisposableSingleObserver<List<Department>> configObserver = new DisposableSingleObserver<List<Department>>() {
                @Override
                public void onSuccess(@NonNull List<Department> configDeps) {
                    binding.loadIndicator.setVisibility(View.GONE);
                    model.setConfigCheck();
                    if (model.checkExisings(configDeps)) checkUserInGroups();
                    else {
                        String message = getString(R.string.deleted_departments);
                        for (String groupId : model.getRemovedDeps(configDeps)) {
                            message += String.format("https://www.facebook.com/groups/%s", groupId);
                        }
                        Alert.show(getContext(), R.string.warning, message);
                    }
                }

                @Override
                public void onError(@NonNull Throwable e) {
                    binding.loadIndicator.setVisibility(View.GONE);
                    Snackbar.make(binding.getRoot(), e.getMessage(), Snackbar.LENGTH_LONG)
                            .setAction(R.string.try_again, view -> downloadConfig())
                            .show();
                }
            };
            Network.downloadConfig(getContext(), configObserver);
        } else checkUserInGroups();
    }

    private void checkUserInGroups() {
        if (!model.isUserFbCheck()) {
            binding.loadIndicator.setVisibility(View.VISIBLE);
            Single.zip(Network.checkInGroups(model.getUserDepartments()), booleen -> !Arrays.asList(booleen).contains(false))
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new DisposableSingleObserver<Boolean>() {
                        @Override
                        public void onSuccess(@NonNull Boolean aBoolean) {
                            binding.loadIndicator.setVisibility(View.GONE);
                            if (aBoolean) {
                                model.setUserFbCheck();
                                prepareUI();
                            } else {
                                DialogInterface.OnClickListener positive = (dialog, i) -> {
                                    dialog.dismiss();
                                    configAccess();
                                };
                                DialogInterface.OnClickListener negative = (dialog, i) -> {
                                    dialog.dismiss();
                                    checkUserInGroups();
                                };
                                Alert.show(getContext(), R.string.warning, R.string.deleted_access, positive, R.string.config, negative, R.string.try_again);
                            }
                        }

                        @Override
                        public void onError(@NonNull Throwable e) {
                            binding.loadIndicator.setVisibility(View.GONE);
                            Snackbar.make(binding.getRoot(), e.getMessage(), Snackbar.LENGTH_LONG)
                                    .setAction(R.string.try_again, view -> checkUserInGroups())
                                    .show();
                        }
                    });
        } else prepareUI();
    }

    private void prepareUI() {
        prepareCardCheck();
        prepareCardAccess();
        prepareCardNetwork();
    }

    private void prepareCardCheck() {
        binding.cardCheckAuto.setVisibility(View.VISIBLE);
        binding.cardCheckManual.setVisibility(View.VISIBLE);
        binding.bScheduleConfig.setText(model.isScheduledOn() ? R.string.off : R.string.on);
        binding.bManualStart.setOnClickListener(v -> startCheck());
        binding.bManualStart.setOnLongClickListener(v -> {
            User.setDownloading(getContext(), false);
            Snackbar.make(binding.getRoot(), R.string.wrong_value, Snackbar.LENGTH_SHORT).show();
            return false;
        });
        binding.bScheduleConfig.setOnClickListener(view -> processSchedule());
        binding.bConfigCheckTime.setOnClickListener(v -> timeChooser());
        binding.bConfigCheckTime.setText(model.getScheduledTime());
        binding.setModel(model);
    }

    private void prepareCardAccess() {
        binding.cardAccess.setVisibility(View.VISIBLE);
        binding.bAccessConfig.setOnClickListener(v -> configAccess());
        if (model.getUserDepartments().size() > 0) {
            DepsAdapter adapter = new DepsAdapter(model.getUserDepartments(), this);
            binding.rAccessList.setLayoutManager(new LinearLayoutManager(getContext()));
            binding.rAccessList.setAdapter(adapter);
        }
    }

    private void prepareCardNetwork() {
        binding.cardNetwork.setVisibility(View.VISIBLE);
        binding.switchAutoUpdate.setChecked(User.isAutoDownload(getContext()));
        binding.switchNetwork.setChecked(User.isWiFiCelluar(getContext()));
        binding.switchAutoUpdate.setOnCheckedChangeListener((buttonView, isChecked) -> User.setAutoDownload(getContext(), isChecked));
        binding.switchNetwork.setOnCheckedChangeListener((buttonView, isChecked) -> User.setWiFiCelluar(getContext(), isChecked));
    }

    private void configAccess() {
        if (!User.isDownloading(getContext())) {
            getActivity().getSupportFragmentManager().popBackStack();
            getActivity().getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.containerView, new Departments())
                    .commit();
        } else Alert.show(getContext(), R.string.warning, R.string.warning_downloading);
    }

    private void startCheck() {
        if (!User.isDownloading(getContext())) {
            model.setLastTimeCheck();
            binding.setModel(model);
            Alert.show(getContext(), R.string.warning, R.string.warning_description);
            getActivity().sendBroadcast(new Intent(MainBroadcast.NOTIFY));
        } else Alert.show(getContext(), R.string.warning, R.string.warning_downloading);
    }

    private void processSchedule() {
        if (!User.isDownloading(getContext())) {
            if (model.isScheduledOn())  scheduleOff();
            else scheduleOn();
        } else Alert.show(getContext(), R.string.warning, R.string.warning_downloading);
    }

    private void scheduleOff() {
        model.setScheduleOn(false);
        binding.setModel(model);
        alarmReceiver.cancelAlarm();
    }

    private void scheduleOn() {
        model.setScheduleOn(true);
        binding.setModel(model);
        getContext().sendBroadcast(new Intent(MainBroadcast.NOTIFY));
        Alert.show(getContext(), R.string.auto_update, R.string.auto_update_description);
        alarmReceiver.setAlarm(model.restoreScheduledHours(), model.restoreScheduledMinutes());
    }

    private void timeChooser() {
        if (!User.isDownloading(getContext())) {
            TimePickerDialog mTimePicker = new TimePickerDialog(getContext(), (timePicker, hourOfDay, minute) -> {
                alarmReceiver.setAlarm(hourOfDay, minute);
                model.setScheduledTime(hourOfDay, minute);
                binding.setModel(model);
            }, model.restoreScheduledHours(), model.restoreScheduledMinutes(), true);
            mTimePicker.setTitle(R.string.choose_time);
            mTimePicker.show();
        } else Alert.show(getContext(), R.string.warning, R.string.warning_downloading);
    }

    private String getVersionNumber() {
        try {
            Activity activity = getActivity();
            PackageInfo packageinfo = activity.getPackageManager().getPackageInfo(activity.getPackageName(), 0);
            return String.format("%s %s", getString(R.string.app_version), packageinfo.versionName);
        } catch (PackageManager.NameNotFoundException ignored) {
            return getString(R.string.error_version);
        }
    }

    @Override
    public void onClick(View view) {
        if (!User.isDownloading(getContext())) {
            int position = (int) view.getTag();
            ArrayList<Department> depsTmp = model.getUserDepartments();
            depsTmp.get(position).selected = false;
            if (Storage.createDepFolders(depsTmp, getContext())) {
                depsTmp.remove(depsTmp.get(position));
                model.setUserDepartments(depsTmp);
                binding.rAccessList.getAdapter().notifyDataSetChanged();
            } else {
                depsTmp.get(position).selected = true;
                Snackbar.make(binding.getRoot(), R.string.error_creating_deleting_folder, Snackbar.LENGTH_LONG).show();
            }
        } else Alert.show(getContext(), R.string.warning, R.string.warning_downloading);
    }
}
