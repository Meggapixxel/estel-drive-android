package com.zhydenko.vadim.esteldrive.service;

import android.app.IntentService;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;

import com.crashlytics.android.Crashlytics;
import com.zhydenko.vadim.esteldrive.R;
import com.zhydenko.vadim.esteldrive.db.BookLogger;
import com.zhydenko.vadim.esteldrive.db.WaitingDownload;
import com.zhydenko.vadim.esteldrive.models.Department;
import com.zhydenko.vadim.esteldrive.models.MyFile;
import com.zhydenko.vadim.esteldrive.models.NewFilesInfo;
import com.zhydenko.vadim.esteldrive.models.User;
import com.zhydenko.vadim.esteldrive.utils.DropboxHelper;
import com.zhydenko.vadim.esteldrive.utils.Network;
import com.zhydenko.vadim.esteldrive.utils.NotificationHandler;
import com.zhydenko.vadim.esteldrive.utils.Storage;
import com.zhydenko.vadim.esteldrive.utils.Utils;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import io.fabric.sdk.android.Fabric;
import io.reactivex.Single;

import static com.zhydenko.vadim.esteldrive.broadcast.MainBroadcast.CONTINUE;
import static com.zhydenko.vadim.esteldrive.broadcast.MainBroadcast.DOWNLOAD;
import static com.zhydenko.vadim.esteldrive.broadcast.MainBroadcast.MAIN_TAG;
import static com.zhydenko.vadim.esteldrive.broadcast.MainBroadcast.NOTIFY;
import static com.zhydenko.vadim.esteldrive.utils.States.CONTINUE_DOWNLOAD;
import static com.zhydenko.vadim.esteldrive.utils.States.COUNT_NOTIFY;
import static com.zhydenko.vadim.esteldrive.utils.Storage.rootFolder;
import static com.zhydenko.vadim.esteldrive.utils.Utils.NOTIFY_ABOUT_CHECKING;
import static com.zhydenko.vadim.esteldrive.utils.Utils.PROGRESS_NOTIFICATION;

public class DropBoxService extends IntentService {

    private NotificationCompat.Builder progress;
    private WaitingDownload waitDownload;
    private BookLogger bookLogger;
    private NotificationHandler nHandler;

    public DropBoxService() {
        super("");
    }

    @Override
    protected void onHandleIntent(final Intent intent) {
        Fabric.with(this, new Crashlytics());
        User.setDownloading(this, true);
        initComponents();
        if (Network.isDeviceOnline(this)) {
            ArrayList<Department> userDeps = User.restoreDepartments(this);
            if (!userDeps.isEmpty()) {
                if (!User.isUserFbCheck(this)) {
                    boolean statusInGroups = Single.zip(Network.checkInGroups(userDeps), booleen -> !Arrays.asList(booleen).contains(false)).blockingGet();
                    if (statusInGroups) {
                        User.setUserFbCheck(this);
                        analyzeIntent(intent, userDeps);
                    } else nHandler.warningAccess();
                } else analyzeIntent(intent, userDeps);
            } else nHandler.warningAccess();
        } else nHandler.warningConnection(intent.getStringExtra(MAIN_TAG));
    }

    private void initComponents() {
        nHandler = new NotificationHandler(this);
        waitDownload = new WaitingDownload(this);
        bookLogger = new BookLogger(this);
    }

    private void analyzeIntent(Intent intent, ArrayList<Department> userDeps) {
        String intentExtra = intent.getStringExtra(MAIN_TAG);
        try {
            if (!intentExtra.equals(CONTINUE_DOWNLOAD.toString())) {
                if (intentExtra.equals(COUNT_NOTIFY.toString())) {
                    NewFilesInfo newFilesInfo = User.getNewFilesInfo(this);
                    if (newFilesInfo == null) newCheck(userDeps);
                    else retryCheck(newFilesInfo, userDeps);
                } else showDownloadProgress(userDeps);
            } else showDownloadProgress(userDeps);
        } catch (Exception e) {
            nHandler.createNotification(getString(R.string.error), e.getMessage(), NOTIFY_ABOUT_CHECKING, null);
        }
    }

    private void newCheck(ArrayList<Department> userDeps) throws Exception {
        nHandler.createNotification(getString(R.string.check), getString(R.string.search_new_files), NOTIFY_ABOUT_CHECKING, null);

        waitDownload.deleteDownloads();

        NewFilesInfo newFilesInfo = getNewFilesInfo(userDeps);
        User.saveDepartments(this, userDeps); // delete
        if (Storage.isInternalMemoryEnough(newFilesInfo)) checkToNotify(newFilesInfo, userDeps);
        else {
            User.setNewFilesInfo(this, newFilesInfo);
            long storage = Storage.getAvailableInternalMemory();
            nHandler.lowMemory(Storage.formatSize(storage), Storage.formatSize(newFilesInfo.getSize()), Storage.formatSize(newFilesInfo.getSize() - storage), NOTIFY);
        }
    }

    private void retryCheck(NewFilesInfo newFilesInfo, ArrayList<Department> userDeps) {
        if (Storage.isInternalMemoryEnough(newFilesInfo)) {
            User.setNewFilesInfo(this, null);
            showDownloadProgress(newFilesInfo, userDeps);
        } else {
            long storage = Storage.getAvailableInternalMemory();
            nHandler.lowMemory(Storage.formatSize(storage), Storage.formatSize(newFilesInfo.getSize()), Storage.formatSize(newFilesInfo.getSize() - storage), NOTIFY);
        }
    }

    private NewFilesInfo getNewFilesInfo(List<Department> userDeps) throws Exception {
        NewFilesInfo newFilesInfo = new NewFilesInfo();
        if (userDeps.size() > 0) return listNewFilesAndNotify(userDeps, newFilesInfo);
        else return newFilesInfo;
    }

    private NewFilesInfo listNewFilesAndNotify(List<Department> userDeps, NewFilesInfo newFilesInfo) throws Exception {
        Department department = userDeps.get(0);
        File depFolder = new File(rootFolder, department.description);
        DropboxHelper dropbox = new DropboxHelper(department, true);

        ArrayList<String> dropboxFolders = dropbox.formattedFolders();
        Storage.analyzeFolders(depFolder, dropboxFolders);

        ArrayList<MyFile> dropboxFiles = dropbox.formattedFiles();
        Storage.analyzeForNewFiles(depFolder, dropboxFiles, waitDownload, newFilesInfo);


        if (userDeps.size() > 1) return listNewFilesAndNotify(userDeps.subList(1, userDeps.size()), newFilesInfo);
        else return newFilesInfo;
    }

    private void checkToNotify(NewFilesInfo newFilesInfo, ArrayList<Department> userDeps) throws Exception {
        if (newFilesInfo.getNewFiles() > 0) {
            String formattedSize = Storage.formatSize(newFilesInfo.getSize());
            String message = String.format("%s: %s (%s)", getString(R.string.new_files), newFilesInfo.getNewFiles(), formattedSize);
            if (User.isAutoDownload(this)) {
                nHandler.createNotification(getString(R.string.check_result), message, NOTIFY_ABOUT_CHECKING, null);
                showDownloadProgress(newFilesInfo, userDeps);
            } else nHandler.createNotification(getString(R.string.tap_to_download), message, NOTIFY_ABOUT_CHECKING, DOWNLOAD);
        } else nHandler.createNotification(getString(R.string.check_result), getString(R.string.no_new_files), NOTIFY_ABOUT_CHECKING, null);
    }

    private void showDownloadProgress(List<Department> userDeps) {
        progress = nHandler.progressNotification(getString(R.string.downloading), waitDownload.getCountToDownload());
        downloadNewFiles(userDeps);
        finish();
    }

    private void showDownloadProgress(NewFilesInfo newFilesInfo, List<Department> userDeps) {
        progress = nHandler.progressNotification(getString(R.string.downloading), newFilesInfo.getNewFiles());
        downloadNewFiles(userDeps);
        finish();
    }

    private void finish() {
        nHandler.cancelNotification(PROGRESS_NOTIFICATION);
        int notDownloaded = waitDownload.getCountToDownload();
        if (notDownloaded == 0) nHandler.createNotification(getString(R.string.download_result), getString(R.string.download_success), NOTIFY_ABOUT_CHECKING, null);
        else {
            String message = String.format("%s: %s", getString(R.string.download_not_all), notDownloaded);
            nHandler.createNotification(getString(R.string.download_result), message, NOTIFY_ABOUT_CHECKING, null);
        }
        User.setDownloading(this, false);
    }

    private void downloadNewFiles(List<Department> userDeps) {
        Department department = userDeps.get(0);
        ArrayList<MyFile> files = waitDownload.getNewFilesForDepartment(department.dropBoxToken);
        if (files.size() > 0) {
            DropboxHelper dropbox = new DropboxHelper(department);
            processedDownload(0, files, dropbox);
        }
        if (userDeps.size() > 1) downloadNewFiles(userDeps.subList(1, userDeps.size()));
    }

    private void processedDownload(int index, List<MyFile> filesToDownload, DropboxHelper dropbox) {
        if (Network.isDeviceOnline(this)) {
            MyFile file = filesToDownload.get(index);
            try {
                String path = dropbox.downloadFile(file);
                waitDownload.deleteDownload(file.getPathDisplay());
                bookLogger.addDownload(file.getName(), true, Utils.getCurrentDate(), path);
                nHandler.updateProgressNotification(progress);
            } catch (Exception e) {
                bookLogger.addDownload(file.getName(), false, Utils.getCurrentDate(), "");
            }
            if (index < filesToDownload.size() - 1) processedDownload(index + 1, filesToDownload, dropbox);
        } else nHandler.warningConnection(CONTINUE);
    }
}