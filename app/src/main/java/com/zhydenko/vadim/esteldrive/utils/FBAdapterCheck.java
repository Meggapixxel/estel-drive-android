package com.zhydenko.vadim.esteldrive.utils;

import com.zhydenko.vadim.esteldrive.models.Department;
import com.zhydenko.vadim.esteldrive.custom_view.CircularProgressButton;

public interface FBAdapterCheck {

    void needCheck(CircularProgressButton button, Department department);

}
