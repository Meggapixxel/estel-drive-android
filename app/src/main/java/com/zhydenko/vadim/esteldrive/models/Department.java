package com.zhydenko.vadim.esteldrive.models;

public class Department {

    public String dropBoxToken, fbGroupId, description, addonPath;
    public boolean selected = false;

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Department) {
            Department dep = (Department) obj;
            return dep.fbGroupId.equals(fbGroupId) && dep.dropBoxToken.equals(dropBoxToken);
        }
        return false;
    }
}
