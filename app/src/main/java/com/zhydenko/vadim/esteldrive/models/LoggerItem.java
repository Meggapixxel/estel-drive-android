package com.zhydenko.vadim.esteldrive.models;

import com.zhydenko.vadim.esteldrive.utils.Utils;

public class LoggerItem {

    private final String name;
    private final String status;
    private final String date;
    private final String path;

    public LoggerItem(String name, String status, long date, String path) {
        this.name = name;
        this.status = status;
        this.date = Utils.getDateString(date);
        this.path = path;
    }

    public String getName() {
        return name;
    }

    public String getStatus() {
        return status;
    }

    public String getDate() {
        return date;
    }

    public String getPath() {
        return path;
    }
}
