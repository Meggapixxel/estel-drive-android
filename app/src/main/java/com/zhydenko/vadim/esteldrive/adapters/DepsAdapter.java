package com.zhydenko.vadim.esteldrive.adapters;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.zhydenko.vadim.esteldrive.R;
import com.zhydenko.vadim.esteldrive.databinding.DepartmentListItemBinding;
import com.zhydenko.vadim.esteldrive.utils.FBAdapterCheck;
import com.zhydenko.vadim.esteldrive.models.Department;

import java.util.List;

public class DepsAdapter extends RecyclerView.Adapter<DepsAdapter.ViewHolder> {

    private final List<Department> deps;
    private final View.OnClickListener listener;
    private FBAdapterCheck checker;

    public DepsAdapter(List<Department> deps, View.OnClickListener listener) {
        this.deps = deps;
        this.listener = listener;
    }

    public DepsAdapter(List<Department> deps, View.OnClickListener listener, FBAdapterCheck checker) {
        this.deps = deps;
        this.listener = listener;
        this.checker = checker;
    }

    @Override
    public DepsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new DepsAdapter.ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.department_list_item, parent, false));
    }

    @Override
    public void onBindViewHolder(DepsAdapter.ViewHolder holder, int position) {
        Department dep = deps.get(position);
        holder.binding.tDepName.setText(dep.description);
        holder.binding.bDep.setTag(position);
        holder.binding.bDep.setOnClickListener(listener);
        holder.binding.bDep.setText(dep.selected ? R.string.delete : R.string.select);
        if (checker != null && dep.selected) checker.needCheck(holder.binding.bDep, dep);
    }

    @Override
    public int getItemCount() {
        return deps.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private DepartmentListItemBinding binding;

        ViewHolder(View itemView) {
            super(itemView);
            binding = DataBindingUtil.bind(itemView);
        }
    }
}