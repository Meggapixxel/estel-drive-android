package com.zhydenko.vadim.esteldrive.broadcast;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.zhydenko.vadim.esteldrive.service.DropBoxService;

import static com.zhydenko.vadim.esteldrive.utils.States.CONTINUE_DOWNLOAD;
import static com.zhydenko.vadim.esteldrive.utils.States.COUNT_NOTIFY;
import static com.zhydenko.vadim.esteldrive.utils.States.NEW_DOWNLOAD;

public class MainBroadcast extends BroadcastReceiver {

    public static final String MAIN_TAG = "MAIN_TAG";
    public static final String DOWNLOAD = "com.zhydenko.vadim.esteldrive.broadcast.MainBroadcast.DOWNLOAD";
    public static final String NOTIFY = "com.zhydenko.vadim.esteldrive.broadcast.MainBroadcast.NOTIFY";
    public static final String CONTINUE = "com.zhydenko.vadim.esteldrive.broadcast.MainBroadcast.CONTINUE";

    @Override
    public void onReceive(Context ctx, Intent intent) {
        Intent request = new Intent(ctx, DropBoxService.class);
        String action = intent.getAction();
        if (action.equalsIgnoreCase(DOWNLOAD)) {
            ctx.startService(request.putExtra(MAIN_TAG, NEW_DOWNLOAD.toString()));
        } else if (action.equalsIgnoreCase(NOTIFY)) {
            ctx.startService(request.putExtra(MAIN_TAG, COUNT_NOTIFY.toString()));
        } else if (action.equals(CONTINUE)) {
            ctx.startService(request.putExtra(MAIN_TAG, CONTINUE_DOWNLOAD.toString()));
        }
    }
}