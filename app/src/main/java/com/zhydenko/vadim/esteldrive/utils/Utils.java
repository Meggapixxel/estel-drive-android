package com.zhydenko.vadim.esteldrive.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Environment;
import android.os.StatFs;

import com.zhydenko.vadim.esteldrive.models.MyFile;

import java.io.File;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class Utils {

    public static final int FUNCTION_1_NOTIFICATION = 1, PROGRESS_NOTIFICATION = 2,
            NO_ACCESS_NOTIFICATION = 3, NO_CONNECTION_NOTIFICATION = 4, NO_MEMORY_NOTIFICATION = 5,
            NOTIFY_ABOUT_CHECKING= 6; //30mb


    @SuppressLint("SimpleDateFormat")
    public static String getDateString() {
        return new SimpleDateFormat("HH:mm:ss dd-MM-yyyy").format(Calendar.getInstance());
    }

    @SuppressLint("SimpleDateFormat")
    public static String getDateString(long time) {
        return new SimpleDateFormat("HH:mm:ss' 'dd-MM-yyyy").format(new Date(time));
    }

    public static long getDateStringMinus(int number) {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_MONTH, -number);
        return calendar.getTime().getTime();
    }

    public static long getCurrentDate() {
        Calendar calendar = Calendar.getInstance();
        return calendar.getTime().getTime();
    }

    public static boolean containsIgnoreCase(String str, ArrayList<String> list) {
        for(String i : list) {
            if (i.equalsIgnoreCase(str)) return true;
        }
        return false;
    }

    public static boolean containsIgnoreCase(MyFile myFile, ArrayList<MyFile> list) {
        for(MyFile i : list) {
            if (myFile.equals(i)) return true;
        }
        return false;
    }

    public static String validPath(String somePath, ArrayList<String> existPathes) {
        for (String path : existPathes) {
            if (path.equalsIgnoreCase(somePath)) return path;
        }
        return somePath;
    }

    public static String convertAllFirstUppercased(String path) {
        if (path != null && path.length() > 0) {
            String test = path.substring(1);
            String[] pathParts = test.split("/");
            if (pathParts.length > 0) {
                String formattedPath = "";
                for (String part : pathParts) {
                    formattedPath += "/" + part.substring(0, 1).toUpperCase() + part.substring(1);
                }
                return formattedPath;
            }
        }
        return path;
    }

}