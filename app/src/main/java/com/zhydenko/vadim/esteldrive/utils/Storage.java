package com.zhydenko.vadim.esteldrive.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.StatFs;
import android.support.v4.content.FileProvider;
import android.webkit.MimeTypeMap;

import com.zhydenko.vadim.esteldrive.R;
import com.zhydenko.vadim.esteldrive.db.WaitingDownload;
import com.zhydenko.vadim.esteldrive.models.Department;
import com.zhydenko.vadim.esteldrive.models.MyFile;
import com.zhydenko.vadim.esteldrive.models.NewFilesInfo;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Function;

public class Storage {

    public static File rootFolder = new File(Environment.getExternalStorageDirectory(), ".EstelDrive");
    private static double kilobyte = 1024,
            megabyte = kilobyte * kilobyte,
            gigabyte = megabyte * megabyte,
            mustHaveSize = 200 * megabyte;


    public static boolean isInternalMemoryEnough(NewFilesInfo newFilesInfo) {
        long availableMemory = getAvailableInternalMemory();
        return availableMemory > newFilesInfo.getSize();
    }

    private static boolean isInternalMemoryEnough() {
        long availableMemory = getAvailableInternalMemory();
        return availableMemory >= (long) mustHaveSize;
    }


    public static long getAvailableInternalMemory() {
        File path = Environment.getDataDirectory();
        StatFs stat = new StatFs(path.getPath());
        long blockSize = stat.getBlockSizeLong();
        long availableBlocks = stat.getAvailableBlocksLong();
        return availableBlocks * blockSize;
    }

    public static boolean createDepFolders(List<Department> departments, Context context) {
        if (Storage.isInternalMemoryEnough()) {
            ArrayList<Boolean> booleen = new ArrayList<>();
            if (rootFolder.exists() || rootFolder.mkdir()) {
                for (Department dep : departments) {
                    File depDir = new File(rootFolder, dep.description);
                    if (dep.selected && !depDir.exists()) booleen.add(depDir.mkdirs());
                    else if (!dep.selected && depDir.exists()) booleen.add(Storage.deleteFolder(depDir));
                    else booleen.add(true);
                }
                Set<Boolean> set = new HashSet<>(booleen);
                return set.size() == 1 ? set.iterator().next() : false;
            } else return false;
        } else Alert.show(context, R.string.warning, R.string.error_memory);
        return false;
    }

    @SuppressLint("DefaultLocale")
    public static String formatSize(Long size) {

        double dob = size.doubleValue();
        if (dob / gigabyte >= 1) return String.format("%.2f Gb", dob / gigabyte);
        else if (dob / megabyte >= 1) return String.format("%.2f Mb", dob / megabyte);
        else if (dob / kilobyte >= 1) return String.format("%.2f Kb", dob / kilobyte);
        else return String.format("%.2f bytes", dob);
    }

    private static ArrayList<MyFile> listFormattedFiles(File root, File folder, ArrayList<MyFile> folderFiles) {
        if (folderFiles != null && folder != null && (folder.exists() || folder.mkdir())) {
            for (File file : folder.listFiles()) {
                if (file.isDirectory()) listFormattedFiles(root, file, folderFiles);
                else {
                    String name = file.getName();
                    String pathDisplay = file.getPath();
                    pathDisplay = pathDisplay.substring(0, pathDisplay.length() - name.length() - 1);
                    pathDisplay = pathDisplay.replace(root.getPath(), "");
                    pathDisplay = Utils.convertAllFirstUppercased(pathDisplay);
                    folderFiles.add(new MyFile(file.getName(), pathDisplay, new Date(file.lastModified()), file.length()));
                }
            }
            return folderFiles;
        } else return null;
    }

    private static ArrayList<String> listFormattedFolders(File root, File folder, ArrayList<String> folderFolders) {
        if (folderFolders != null && folder != null && (folder.exists() || folder.mkdir())) {
            for (File file : folder.listFiles()) {
                if (file.isDirectory()) {
                    folderFolders.add(file.getPath().replace(root.getPath(), ""));
                    listFormattedFolders(root, file, folderFolders);
                }
            }
            return folderFolders;
        } else return null;
    }

    public static void analyzeFolders(File rootFolder, ArrayList<String> dropboxFolders) throws Exception {
        ArrayList<String> userFolders = listFormattedFolders(rootFolder, rootFolder, new ArrayList<>());

        for (String folder : userFolders) {
            if (!Utils.containsIgnoreCase(folder, dropboxFolders)) {
                File folderToDelete = new File(rootFolder, folder);
                if (folderToDelete.exists() && !deleteFolder(folderToDelete)) throw new Exception("Error deleting folder");
            } else dropboxFolders.remove(folder);
        }

        for (String folder : dropboxFolders) {
            File folderToCreate = new File(rootFolder, folder);
            if (!folderToCreate.exists() && !folderToCreate.mkdirs()) throw new Exception("Error creating folder");
        }
    }

    public static void analyzeForNewFiles(File rootFolder, ArrayList<MyFile> dropboxFiles, WaitingDownload waitingDownload, NewFilesInfo newFilesInfo) throws Exception {
        ArrayList<String> existFolders = listFormattedFolders(rootFolder, rootFolder, new ArrayList<>());




        ArrayList<MyFile> userFiles = listFormattedFiles(rootFolder, rootFolder, new ArrayList<>());

        for (MyFile file : userFiles) {
            if (!dropboxFiles.contains(file)) {
                // TODO testing
                File fileToDelete = new File(rootFolder, file.getPathDisplay() + file.getName());
                if (fileToDelete.exists() && !fileToDelete.delete()) throw new Exception("Error deleting file");
            } else {
                int index = dropboxFiles.indexOf(file);
                if (file.getChangeDate().after(dropboxFiles.get(index).getChangeDate())) dropboxFiles.remove(file);
            }
        }

        for (MyFile file : dropboxFiles) {
            file.setPathDisplay(Utils.validPath(file.getPathDisplay(), existFolders));
            newFilesInfo.add(file.getRealSize());
            waitingDownload.addDownload(file);
        }
    }

    public static boolean deleteFolder(File file) {
        File[] contents = file.listFiles();
        if (contents != null) {
            for (File f : contents) {
                deleteFolder(f);
            }
        }
        return file.delete();
    }

    public static boolean deleteFile(String filePath) {
        if (!filePath.equals("")) {
            File file = new File(filePath);
            if (file.exists()) {
                return file.delete();
            }
        }
        return false;
    }

    public static void openFile(Context context, String filePath) {
        if (!filePath.equals("")) {
            File file = new File(filePath);
            if (file.exists()) {
                int lastIndex = file.getName().lastIndexOf(".");
                if (lastIndex > 15) {
                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                        Uri apkUri = FileProvider.getUriForFile(context, "com.zhydenko.vadim.esteldrive.fileprovider", file);
                        intent.setData(apkUri);
                        intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                    } else {
                        Uri apkUri = Uri.fromFile(file);
                        String mimeType = MimeTypeMap.getFileExtensionFromUrl(apkUri.toString());
                        intent.setDataAndType(apkUri, MimeTypeMap.getSingleton().getMimeTypeFromExtension(mimeType));
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    }


                    PackageManager packageManager = context.getPackageManager();
                    if (intent.resolveActivity(packageManager) != null) {
                        context.startActivity(intent);
                    } else Alert.show(context, R.string.warning, R.string.no_app_view);


                } else Alert.show(context, R.string.warning, R.string.file_no_ext);
            } else Alert.show(context, R.string.warning, R.string.file_not_exists);
        } else Alert.show(context, R.string.warning, R.string.file_not_found);
    }

}