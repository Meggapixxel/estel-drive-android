package com.zhydenko.vadim.esteldrive.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.zhydenko.vadim.esteldrive.models.MyFile;

import java.util.ArrayList;
import java.util.Date;

public class WaitingDownload extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 3;
    private static final String DATABASE_NAME = "WaitDownload.db";
    private static final String TABLE_DOWNLOADS = "Downloads";
    private static final String KEY_ID = "id";
    private static final String KEY_NAME = "name";
    private static final String KEY_PATH_DISPLAY = "pathDisplay";
    private static final String KEY_CHANGE_DATE = "changeDate";
    private static final String KEY_REAL_SIZE = "real_size" ;
    private static final String KEY_DROPBOX_ID = "dropbox_id";
    private static final String KEY_DROPBOX_TOKEN = "dropbox_token";

    public WaitingDownload(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_DOWNLOADS_TABLE = "CREATE TABLE " + TABLE_DOWNLOADS + "("
                + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + KEY_NAME + " TEXT, "
                + KEY_PATH_DISPLAY + " TEXT, "
                + KEY_CHANGE_DATE + " INTEGER, "
                + KEY_REAL_SIZE + " TEXT, "
                + KEY_DROPBOX_ID + " TEXT, "
                + KEY_DROPBOX_TOKEN + " TEXT );";
        db.execSQL(CREATE_DOWNLOADS_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_DOWNLOADS);
        onCreate(db);
    }

    public int getCountToDownload() {
        ArrayList<MyFile> arrayList = new ArrayList<>();
        SQLiteDatabase db = getWritableDatabase();

        Cursor cursor = db.query( TABLE_DOWNLOADS,
                new String[] { KEY_NAME, KEY_PATH_DISPLAY, KEY_CHANGE_DATE, KEY_REAL_SIZE, KEY_DROPBOX_ID, KEY_DROPBOX_TOKEN },
                null, null, null, null, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            MyFile myFile = cursorToContent(cursor);
            arrayList.add(myFile);
            cursor.moveToNext();
        }

        cursor.close();
        db.close();
        return arrayList.size();
    }

    public void addDownload(MyFile myFile) {
        ContentValues values = new ContentValues();
        values.put(KEY_NAME, myFile.getName());
        values.put(KEY_PATH_DISPLAY, myFile.getPathDisplay());
        values.put(KEY_CHANGE_DATE, myFile.getChangeDate().getTime());
        values.put(KEY_REAL_SIZE, String.valueOf(myFile.getRealSize()));
        values.put(KEY_DROPBOX_ID, myFile.getDropboxFileId());
        values.put(KEY_DROPBOX_TOKEN, myFile.getDropboxToken());
        SQLiteDatabase db = getWritableDatabase();
        db.insert(TABLE_DOWNLOADS, null, values);
        db.close();
    }

    public void deleteDownload(String pathDiplay) {
        SQLiteDatabase db = getWritableDatabase();
        db.execSQL("DELETE FROM " + TABLE_DOWNLOADS + " WHERE " + KEY_PATH_DISPLAY + "=\"" + pathDiplay + "\";");
        db.close();
    }

    public void deleteDownloads() {
        SQLiteDatabase db = getWritableDatabase();
        db.execSQL("DELETE FROM " + TABLE_DOWNLOADS);
        db.close();
    }

    public ArrayList<MyFile> getListFilesToDownload() {
        ArrayList<MyFile> arrayList = new ArrayList<>();
        SQLiteDatabase db = getWritableDatabase();

        Cursor cursor = db.query(TABLE_DOWNLOADS,
                new String[] { KEY_NAME, KEY_PATH_DISPLAY, KEY_CHANGE_DATE, KEY_REAL_SIZE, KEY_DROPBOX_ID, KEY_DROPBOX_TOKEN },
                null, null, null, null, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            MyFile myFile = cursorToContent(cursor);
            arrayList.add(myFile);
            cursor.moveToNext();
        }

        cursor.close();
        db.close();
        return arrayList;
    }

    public ArrayList<MyFile> getNewFilesForDepartment(String dropboxToken) {
        ArrayList<MyFile> arrayList = new ArrayList<>();
        SQLiteDatabase db = getWritableDatabase();

        Cursor cursor2 = db.rawQuery("SELECT * FROM " + TABLE_DOWNLOADS + " WHERE " + KEY_DROPBOX_TOKEN + " = '" + dropboxToken + "';", null);
        cursor2.moveToFirst();
        while (!cursor2.isAfterLast()) {
            MyFile myFile = cursorToContent(cursor2);
            arrayList.add(myFile);
            cursor2.moveToNext();
        }

        cursor2.close();
        db.close();

        return arrayList;
    }

    private MyFile cursorToContent(Cursor c) {
        return new MyFile(c.getString(c.getColumnIndex(KEY_NAME)),
                c.getString(c.getColumnIndex(KEY_PATH_DISPLAY)),
                new Date(c.getLong(c.getColumnIndex(KEY_CHANGE_DATE))),
                Long.parseLong(c.getString(c.getColumnIndex(KEY_REAL_SIZE))),
                c.getString(c.getColumnIndex(KEY_DROPBOX_ID)),
                c.getString(c.getColumnIndex(KEY_DROPBOX_TOKEN)));
    }
}
