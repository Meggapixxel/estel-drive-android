package com.zhydenko.vadim.esteldrive.utils;

import android.app.Activity;
import android.view.View;

import co.mobiwise.materialintro.shape.Focus;
import co.mobiwise.materialintro.shape.FocusGravity;
import co.mobiwise.materialintro.view.MaterialIntroView;

public class Intro {

    public static void show(Activity activity, View view, String text, String id) {
        new MaterialIntroView.Builder(activity)
                .enableDotAnimation(true)
                .enableIcon(false)
                .setFocusGravity(FocusGravity.CENTER)
                .setFocusType(Focus.NORMAL)
                .setDelayMillis(500)
                .performClick(false)
                .enableFadeAnimation(true)
                .setInfoText(text)
                .setTarget(view)
                .setUsageId(id)
                .show();
    }
}
