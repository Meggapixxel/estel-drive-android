package com.zhydenko.vadim.esteldrive.models;

public class NewFilesInfo {

    private long size = 0L;
    private int newFiles = 0;

    public void add(long size) {
        this.size += size;
        newFiles++;
    }

    public int getNewFiles() {
        return newFiles;
    }

    public long getSize() {
        return size;
    }

}
