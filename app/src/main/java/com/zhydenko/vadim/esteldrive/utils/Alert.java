package com.zhydenko.vadim.esteldrive.utils;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;

import com.zhydenko.vadim.esteldrive.R;

public class Alert {

    public static void show(Context context, int title, int message) {
        new AlertDialog.Builder(context)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton(R.string.ok, (dialog, which) -> dialog.dismiss())
                .show();
    }

    public static void show(Context context, int title, String message) {
        new AlertDialog.Builder(context)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton(R.string.ok, (dialog, which) -> dialog.dismiss())
                .show();
    }

    public static void show(Context context, int title, int message,
                            DialogInterface.OnClickListener positiveClick, int positiveTitle,
                            DialogInterface.OnClickListener negativeClick, int negativeTitle) {
        new AlertDialog.Builder(context)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton(positiveTitle, positiveClick)
                .setNegativeButton(negativeTitle, negativeClick)
                .show();
    }
}
