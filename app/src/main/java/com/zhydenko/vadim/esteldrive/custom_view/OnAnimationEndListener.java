package com.zhydenko.vadim.esteldrive.custom_view;

interface OnAnimationEndListener {

    void onAnimationEnd();

}
