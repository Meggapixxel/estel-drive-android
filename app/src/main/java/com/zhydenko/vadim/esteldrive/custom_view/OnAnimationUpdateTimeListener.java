package com.zhydenko.vadim.esteldrive.custom_view;

interface OnAnimationUpdateTimeListener {

    void onAnimationTimeUpdate(int timeElapsed);

}

