package com.zhydenko.vadim.esteldrive.utils;

import com.dropbox.core.DbxException;
import com.dropbox.core.DbxRequestConfig;
import com.dropbox.core.v2.DbxClientV2;
import com.dropbox.core.v2.files.FileMetadata;
import com.dropbox.core.v2.files.FolderMetadata;
import com.dropbox.core.v2.files.Metadata;
import com.zhydenko.vadim.esteldrive.models.Department;
import com.zhydenko.vadim.esteldrive.models.MyFile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import static com.zhydenko.vadim.esteldrive.utils.Storage.rootFolder;

public class DropboxHelper {

    private final DbxRequestConfig requestConfig = new DbxRequestConfig("dropbox/estelfiles");
    private final DbxClientV2 dropbox;
    private String homePath = null;
    private Department department;
    private ArrayList<MyFile> formattedFiles = new ArrayList<>();
    private ArrayList<String> formattedFolders = new ArrayList<>();

    public DropboxHelper(Department department, boolean scan) throws DbxException {
        this.dropbox = new DbxClientV2(requestConfig, department.dropBoxToken);
        this.department = department;
        if (scan) listFormattedFileFolders(department.addonPath);
    }

    public DropboxHelper(String token, String homePath, boolean scan) throws DbxException {
        this.dropbox = new DbxClientV2(requestConfig, token);
        this.homePath = homePath;
        if (scan) listFormattedFileFolders(homePath);
    }

    public DropboxHelper(Department department) {
        this.dropbox = new DbxClientV2(requestConfig, department.dropBoxToken);
        this.department = department;
    }

    public void update() throws DbxException {
        listFormattedFileFolders(homePath != null ? homePath : "");
    }

    private void listFormattedFileFolders(String path) throws DbxException {
        List<Metadata> metas = dropbox.files().listFolder(path).getEntries();
        for (Metadata meta : metas) {
            if (meta instanceof FolderMetadata) {
                formattedFolders.add(Utils.convertAllFirstUppercased(meta.getPathDisplay()));
                listFormattedFileFolders(meta.getPathLower());
            } else formattedFiles.add(convert((FileMetadata) meta));
        }
    }

    private MyFile convert(FileMetadata file) {
        String name = file.getName();
        String pathDisplay = file.getPathDisplay();
        pathDisplay = pathDisplay.substring(0, pathDisplay.length() - name.length() - 1);
        pathDisplay = Utils.convertAllFirstUppercased(pathDisplay);
        return new MyFile(file.getName(), pathDisplay, file.getClientModified(), file.getSize(), file.getId(), department.dropBoxToken);
    }

    public ArrayList<MyFile> formattedFiles() {
        return formattedFiles;
    }

    public ArrayList<String> formattedFolders() {
        return formattedFolders;
    }

    public String downloadFile(MyFile myFile) throws Exception {
        File file = new File(rootFolder, department.description + myFile.getPathDisplay());
        if (file.exists() || file.mkdirs()) {
            file = new File(file, myFile.getName());
            if (file.createNewFile()) {
                OutputStream outputStream = new FileOutputStream(file);
                dropbox.files().download(myFile.getDropboxFileId()).download(outputStream);
            } else throw new Exception("Error creation file"); // TODO replace text
        }
        return file.getPath();
    }

}
