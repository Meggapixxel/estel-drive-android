package com.zhydenko.vadim.esteldrive.models;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;

public class User {

    public static final String SHARED_PREF_NAME = "ESTELDrive";
    private static final String USER_DEPARTMENTS = "USER_DEPARTMENTS",
                                USER_SETUP = "USER_SETUP",
                                USER_CONFIG_CHECK = "USER_CONFIG_CHECK",
                                USER_FB_CHECK = "USER_FB_CHECK",
                                USER_DOWNLOADING = "USER_DOWNLOADING",
                                NEW_FILES_INFO = "NEW_FILES_INFO",
                                AUTO_DOWNLOAD = "AUTO_DOWNLOAD",
                                ADDITION_CONFIG = "ADDITION_CONFIG",
                                SCHEDULED_HOURS = "SCHEDULED_HOURS",
                                SCHEDULED_MINUTES = "SCHEDULED_MINUTES",
                                LAST_TIME_CHECK = "lastTimeCheck",
                                SCHEDULED = "SCHEDULED",
                                WIFI_ONLY = "WIFI_ONLY";
    private SharedPreferences sharedPreferences;
    private String version, scheduledTime, lastTimeCheck, configCheck, fbCheck;
    private boolean scheduleOn;
    private ArrayList<Department> userDepartments;

    public User(SharedPreferences sharedPreferences, String version) {
        this.sharedPreferences = sharedPreferences;
        this.scheduleOn = restoreScheduledOn();
        this.lastTimeCheck = restoreLastTimeCheck();
        this.scheduledTime = restoreScheduledTime();
        this.userDepartments = restoreDepartments();
        this.configCheck = restoreConfigCheck();
        this.fbCheck = restoreUserFbCheck();
        this.version = version;
    }

    public User(SharedPreferences sharedPreferences) {
        this.sharedPreferences = sharedPreferences;
    }

    private boolean restoreScheduledOn() {
        return sharedPreferences.getBoolean(SCHEDULED, false);
    }
    public boolean isScheduledOn() {
        return  scheduleOn;
    }
    public void setScheduleOn(boolean scheduleOn) {
        sharedPreferences.edit().putBoolean(SCHEDULED, scheduleOn).apply();
        this.scheduleOn = scheduleOn;
    }


    private String restoreLastTimeCheck() {
        return sharedPreferences.getString(LAST_TIME_CHECK, "");
    }
    public String getLastTimeCheck() {
        return lastTimeCheck;
    }
    public void setLastTimeCheck() {
        this.lastTimeCheck = getDateString();
        sharedPreferences.edit().putString(LAST_TIME_CHECK, this.lastTimeCheck).apply();
    }


    private String restoreScheduledTime() {
        return String.format("%s:%s", processTimeForView(restoreScheduledHours()), processTimeForView(restoreScheduledMinutes()));
    }
    public String getScheduledTime() {
        return scheduledTime;
    }
    public void setScheduledTime(int hours, int minutes) {
        sharedPreferences.edit().putInt(SCHEDULED_HOURS, hours).apply();
        sharedPreferences.edit().putInt(SCHEDULED_MINUTES, minutes).apply();
        this.scheduledTime = String.format("%s:%s", processTimeForView(hours), processTimeForView(minutes));
    }
    public int restoreScheduledHours() {
        return sharedPreferences.getInt(SCHEDULED_HOURS, 20);
    }
    public int restoreScheduledMinutes() {
        return sharedPreferences.getInt(SCHEDULED_MINUTES, 0);
    }
    public static int restoreScheduledHours(Context context) {
        return context.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE).getInt(SCHEDULED_HOURS, 20);
    }
    public static int restoreScheduledMinutes(Context context) {
        return context.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE).getInt(SCHEDULED_MINUTES, 0);
    }


    public ArrayList<Department> getUserDepartments() {
        return userDepartments;
    }
    public void setUserDepartments(ArrayList<Department> userDepartments) {
        Gson gson = new Gson();
        String json = gson.toJson(userDepartments);
        sharedPreferences.edit().putString(USER_DEPARTMENTS, json).apply();
        this.userDepartments = userDepartments;
    }
    private ArrayList<Department> restoreDepartments() {
        String deps = sharedPreferences.getString("USER_DEPARTMENTS", null);
        if (deps != null) {
            Gson gson = new Gson();
            return new ArrayList<>(Arrays.asList(gson.fromJson(deps, Department[].class)));
        }
        return new ArrayList<>();
    }
    public void removeDepartment(int position) {
        userDepartments.remove(position);
        setUserDepartments(userDepartments);
    }
    public static ArrayList<Department> restoreDepartments(Context context) {
        String deps = context.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE).getString(USER_DEPARTMENTS, null);
        if (deps != null) {
            Gson gson = new Gson();
            return new ArrayList<>(Arrays.asList(gson.fromJson(deps, Department[].class)));
        }
        return new ArrayList<>();
    }
    public static void saveDepartments(Context context, List<Department> departments) {
        departments = Observable.fromIterable(departments).filter(b -> b.selected).toList().blockingGet();
        Gson gson = new Gson();
        String json = gson.toJson(departments);
        SharedPreferences shared = context.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = shared.edit();
        editor.putString(USER_DEPARTMENTS, json);
        editor.apply();
    }


    public String getConfigCheck() {
        return configCheck;
    }
    public void setConfigCheck() {
        String configCheck = getCheckFormatter().format(new Date());
        sharedPreferences.edit().putString(USER_CONFIG_CHECK, configCheck).apply();
        this.configCheck = configCheck;
    }
    private String restoreConfigCheck() {
        return sharedPreferences.getString(USER_CONFIG_CHECK, null);
    }
    public boolean isCheckConfig() {
        String init = getConfigCheck();
        String newValue = getCheckFormatter().format(new Date());
        return init != null && init.equals(newValue);
    }


    public String getUserFbCheck() {
        return fbCheck;
    }
    public void setUserFbCheck() {
        String fbCheck = getCheckFormatter().format(new Date());
        sharedPreferences.edit().putString(USER_FB_CHECK, fbCheck).apply();
        this.fbCheck = fbCheck;
    }
    private String restoreUserFbCheck() {
        return sharedPreferences.getString(USER_CONFIG_CHECK, null);
    }
    public boolean isUserFbCheck() {
        String init = getUserFbCheck();
        String newValue = getCheckFormatter().format(new Date());
        return init != null && init.equals(newValue);
    }
    public static void setUserFbCheck(Context context) {
        String fbCheck = getCheckFormatter().format(new Date());
        context.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE).edit().putString(USER_FB_CHECK, fbCheck).apply();
    }
    public static boolean isUserFbCheck(Context context) {
        String init = context.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE).getString(USER_FB_CHECK, null);
        String newValue = getCheckFormatter().format(new Date());
        return init != null && init.equals(newValue);
    }


    public static boolean isUserSetup(Context context) {
        return context.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE).getBoolean(USER_SETUP, false);
    }
    public static void setUserSetup(Context context) {
        context.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE).edit().putBoolean(USER_SETUP, true).apply();
    }


    public static boolean isDownloading(Context context) {
        return context.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE).getBoolean(USER_DOWNLOADING, false);
    }
    public static void setDownloading(Context context, boolean status) {
        context.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE).edit().putBoolean(USER_DOWNLOADING, status).apply();
    }


    public static boolean isAdditionConfig(Context context) {
        return context.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE).getBoolean(ADDITION_CONFIG, false);
    }
    public static void setAdditionConfig(Context context, boolean status) {
        context.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE).edit().putBoolean(ADDITION_CONFIG, status).apply();
    }


    public static boolean isAutoDownload(Context context) {
        return context.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE).getBoolean(AUTO_DOWNLOAD, true);
    }
    public static void setAutoDownload(Context context, boolean status) {
        context.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE).edit().putBoolean(AUTO_DOWNLOAD, status).apply();
    }


    public static boolean isWiFiCelluar(Context context) {
        return context.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE).getBoolean(WIFI_ONLY, false);
    }
    public static void setWiFiCelluar(Context context, boolean status) {
        context.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE).edit().putBoolean(WIFI_ONLY, status).apply();
    }


    public static NewFilesInfo getNewFilesInfo(Context context) {
        Gson gson = new Gson();
        String json = context.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE).getString(NEW_FILES_INFO, "");
        return gson.fromJson(json, NewFilesInfo.class);
    }
    public static void setNewFilesInfo(Context context, NewFilesInfo newFilesInfo) {
        Gson gson = new Gson();
        String json = gson.toJson(newFilesInfo);
        context.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE).edit().putString(NEW_FILES_INFO, json).apply();
    }


    public String getVersion() {
        return version;
    }


    private String processTimeForView(int value){
        return value < 10 ? String.format("0%s", value) : String.valueOf(value);
    }


    @SuppressLint("SimpleDateFormat")
    private String getDateString() {
        return new SimpleDateFormat("HH:mm' 'dd-MM-yyyy").format(new Date());
    }


    public static SimpleDateFormat getCheckFormatter() {
        return new SimpleDateFormat("ddMMyyyy");
    }


    public boolean checkExisings(List<Department> configDeps) {
        for (Department dep : userDepartments) {
            if (!configDeps.contains(dep)) return false;
        }
        return true;
    }

    public List<String> getRemovedDeps(List<Department> configDeps) {
        List<String> removedDeps = new ArrayList<>();
        userDepartments.stream().filter(dep -> !configDeps.contains(dep)).forEach(dep -> {
            removedDeps.add(dep.fbGroupId);
            userDepartments.remove(dep);
        });
        setUserDepartments(userDepartments);
        return removedDeps;
    }
}
