package com.zhydenko.vadim.esteldrive.utils;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;

import com.zhydenko.vadim.esteldrive.Main;
import com.zhydenko.vadim.esteldrive.R;
import com.zhydenko.vadim.esteldrive.models.User;

import static com.zhydenko.vadim.esteldrive.utils.Utils.NO_ACCESS_NOTIFICATION;
import static com.zhydenko.vadim.esteldrive.utils.Utils.NO_CONNECTION_NOTIFICATION;
import static com.zhydenko.vadim.esteldrive.utils.Utils.NO_MEMORY_NOTIFICATION;
import static com.zhydenko.vadim.esteldrive.utils.Utils.PROGRESS_NOTIFICATION;

public class NotificationHandler extends ContextWrapper {

//    private static final String ANDROID_CHANNEL_ID = "com.zhydenko.vadim.esteldrive.notification.android";
//    private static final String ANDROID_CHANNEL_NAME = "All notifications";
    private static NotificationManager mNotificationManager;
    private int currentProgress = 0;
    private int currentAll = 0;

    public NotificationHandler(Context base) {
        super(base);
//        createChannel();
    }

    public NotificationManager getManager() {
        if (mNotificationManager == null) {
            mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        }
        return mNotificationManager;
    }

//    public void createChannel() {
//        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
//            NotificationChannel androidChannel = new NotificationChannel(ANDROID_CHANNEL_ID, ANDROID_CHANNEL_NAME, NotificationManager.IMPORTANCE_DEFAULT);
//            androidChannel.enableVibration(true);
//            androidChannel.setLockscreenVisibility(Notification.VISIBILITY_PRIVATE);
//            getManager().createNotificationChannel(androidChannel);
//        }
//    }

//    @RequiresApi(api = Build.VERSION_CODES.O)
//    public Notification.Builder getChannelNotification(String title, String text) {
//        return new Notification.Builder(getApplicationContext(), ANDROID_CHANNEL_ID)
//                .setContentTitle(title)
//                .setContentText(text)
//                .setSmallIcon(android.R.drawable.stat_notify_more)
//                .setAutoCancel(true);
//    }

    public NotificationCompat.Builder getNotification(String title, String text) {
        return new NotificationCompat.Builder(getApplicationContext())
                .setSmallIcon(android.R.drawable.stat_notify_more)
                .setWhen(System.currentTimeMillis())
                .setDefaults(Notification.DEFAULT_LIGHTS | Notification.DEFAULT_SOUND | Notification.DEFAULT_VIBRATE)
                .setAutoCancel(false)
                .setContentTitle(title)
                .setContentText(text);
    }


    public void createNotification(String title, String text, int notifyId, String action) {
//        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
//            Notification.Builder nb = getChannelNotification(title, text);
//            getManager().notify(notifyId, nb.build());
//        } else {
        NotificationCompat.Builder nb = getNotification(title, text);
        if (action != null) nb.setContentIntent(PendingIntent.getBroadcast(this, 0, new Intent(action), PendingIntent.FLAG_UPDATE_CURRENT));
        getManager().notify(notifyId, nb.build());
//        }
    }

    public NotificationCompat.Builder progressNotification(String title, int amount) {
        currentProgress = 0;
        currentAll = amount;
        NotificationCompat.Builder nb = new NotificationCompat.Builder(getApplicationContext())
                .setSmallIcon(android.R.drawable.stat_notify_more)
                .setWhen(System.currentTimeMillis())
                .setAutoCancel(false)
                .setContentTitle(title)
                .setContentText(null)
                .setOngoing(true)
                .setProgress(currentAll, currentProgress, false);
        getManager().notify(PROGRESS_NOTIFICATION, nb.build());
        return nb;
    }

    public void updateProgressNotification(NotificationCompat.Builder nb) {
        nb.setProgress(currentAll, ++currentProgress, false);
        mNotificationManager.notify(PROGRESS_NOTIFICATION, nb.build());
    }

    public void warningConnection(String action) {
        Intent intent = new Intent(action);
        NotificationCompat.Builder nb = getNotification(getString(R.string.no_connection), getString(R.string.tap_to_retry));
        nb.setOngoing(true).setContentIntent(PendingIntent.getBroadcast(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT));
        getManager().notify(NO_CONNECTION_NOTIFICATION, nb.build());
    }

    public void warningAccess() {
        Intent intent = new Intent(this, Main.class);
        User.setAdditionConfig(this, true);
        NotificationCompat.Builder nb = getNotification(getString(R.string.warning), getString(R.string.errors_in_deps_config));
        nb.setOngoing(true).setContentIntent(PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT));
        getManager().notify(NO_ACCESS_NOTIFICATION, nb.build());
    }

    public void lowMemory(String availableSize, String needSize, String freeSize, String action) {
        String message = String.format("%s %s, %s: %s, %s: %s (%s)",
                getString(R.string.available), availableSize,
                getString(R.string.need_to_download), needSize,
                getString(R.string.need_to_free), freeSize,
                getString(R.string.tap_when_freed));
        Intent intent = new Intent(action);
        NotificationCompat.Builder nb = getNotification(getString(R.string.warning), message);
        nb.setOngoing(true).setContentIntent(PendingIntent.getBroadcast(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT));
        getManager().notify(NO_MEMORY_NOTIFICATION, nb.build());
    }

    public void cancelNotification(int id) {
        mNotificationManager.cancel(id);
    }

    public void cancelAllNotification() { mNotificationManager.cancelAll(); }

    public void clearAllExceptDownloadAndNewFilesNotification() {
        mNotificationManager.cancel(NO_ACCESS_NOTIFICATION);
        mNotificationManager.cancel(NO_CONNECTION_NOTIFICATION);
        mNotificationManager.cancel(NO_MEMORY_NOTIFICATION);
    }
}
