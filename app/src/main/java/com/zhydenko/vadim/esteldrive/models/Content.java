package com.zhydenko.vadim.esteldrive.models;

import android.graphics.drawable.Drawable;

import java.util.Date;

public class Content {

	private String name, path, pathInSdFolder, convertedSize;
	private Date changeDate;
	private boolean isDir;
	private long realSize;

	public Content(String name, String path, String pathInSdFolder, Date changeDate, String convertedSize, boolean isDir, long realSize) {
		super();
		this.name = name;
		this.path = path;
		this.pathInSdFolder = pathInSdFolder;
		this.changeDate = changeDate;
		this.convertedSize = convertedSize;
		this.isDir = isDir;
		this.realSize = realSize;
	}

	public String getName() { return name; }

	public String getPath() { return path; }

	public String getPathInSdFolder() { return pathInSdFolder; }

	public void setPathInSdFolder(String pathInSdFolder) { this.pathInSdFolder = pathInSdFolder; }

	public Date getChangeDate() { return changeDate; }

	public String getConvertedSize() { return convertedSize; }

	public long getRealSize() { return realSize; }

	public boolean isDir() { return isDir; }
}
