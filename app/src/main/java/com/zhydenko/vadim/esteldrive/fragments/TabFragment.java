package com.zhydenko.vadim.esteldrive.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.zhydenko.vadim.esteldrive.R;

import java.util.ArrayList;

public class TabFragment extends Fragment {

    TabLayout tabLayout;
    ViewPager viewPager;
    ArrayList<Fragment> fragmentList = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_tab, null);

        tabLayout = (TabLayout) view.findViewById(R.id.tabs);
        viewPager = (ViewPager) view.findViewById(R.id.viewpager);
        fragmentList.add(new FileFragment());
        fragmentList.add(new MainFragment());
        fragmentList.add(new LoggerFragment());
        viewPager.setAdapter(new MyAdapter(getChildFragmentManager()));
        viewPager.setCurrentItem(1);
        tabLayout.post(() -> tabLayout.setupWithViewPager(viewPager));
        return view;
    }

    private class MyAdapter extends FragmentPagerAdapter {

        MyAdapter(FragmentManager fm){
            super(fm);
        }

        @Override
        public Fragment getItem(int position){
            switch (position){
                case 0 : return fragmentList.get(0);
                case 1 : return fragmentList.get(1);
                case 2 : return fragmentList.get(2);
                default: return null;
            }
        }

        @Override
        public int getCount(){
            return fragmentList.size();
        }

        @Override
        public CharSequence getPageTitle(int position){
            switch (position){
                case 0 : return getString(R.string.tab_files);
                case 1 : return getString(R.string.tab_main);
                case 2 : return  getString(R.string.tab_logger);
                default: return null;
            }
        }
    }
}
