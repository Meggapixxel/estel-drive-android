package com.zhydenko.vadim.esteldrive.databinding;
import com.zhydenko.vadim.esteldrive.R;
import com.zhydenko.vadim.esteldrive.BR;
import android.view.View;
public class FragmentDepartmentsBinding extends android.databinding.ViewDataBinding  {

    private static final android.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.fbLogin, 1);
        sViewsWithIds.put(R.id.rDepsList, 2);
        sViewsWithIds.put(R.id.bDepsAccept, 3);
        sViewsWithIds.put(R.id.pDepLoading, 4);
        sViewsWithIds.put(R.id.tDepSelection, 5);
    }
    // views
    public final android.widget.Button bDepsAccept;
    public final com.facebook.login.widget.LoginButton fbLogin;
    private final android.support.constraint.ConstraintLayout mboundView0;
    public final android.widget.ProgressBar pDepLoading;
    public final android.support.v7.widget.RecyclerView rDepsList;
    public final android.widget.TextView tDepSelection;
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public FragmentDepartmentsBinding(android.databinding.DataBindingComponent bindingComponent, View root) {
        super(bindingComponent, root, 0);
        final Object[] bindings = mapBindings(bindingComponent, root, 6, sIncludes, sViewsWithIds);
        this.bDepsAccept = (android.widget.Button) bindings[3];
        this.fbLogin = (com.facebook.login.widget.LoginButton) bindings[1];
        this.mboundView0 = (android.support.constraint.ConstraintLayout) bindings[0];
        this.mboundView0.setTag(null);
        this.pDepLoading = (android.widget.ProgressBar) bindings[4];
        this.rDepsList = (android.support.v7.widget.RecyclerView) bindings[2];
        this.tDepSelection = (android.widget.TextView) bindings[5];
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x1L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    public boolean setVariable(int variableId, Object variable) {
        switch(variableId) {
        }
        return false;
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        // batch finished
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;

    public static FragmentDepartmentsBinding inflate(android.view.LayoutInflater inflater, android.view.ViewGroup root, boolean attachToRoot) {
        return inflate(inflater, root, attachToRoot, android.databinding.DataBindingUtil.getDefaultComponent());
    }
    public static FragmentDepartmentsBinding inflate(android.view.LayoutInflater inflater, android.view.ViewGroup root, boolean attachToRoot, android.databinding.DataBindingComponent bindingComponent) {
        return android.databinding.DataBindingUtil.<FragmentDepartmentsBinding>inflate(inflater, com.zhydenko.vadim.esteldrive.R.layout.fragment_departments, root, attachToRoot, bindingComponent);
    }
    public static FragmentDepartmentsBinding inflate(android.view.LayoutInflater inflater) {
        return inflate(inflater, android.databinding.DataBindingUtil.getDefaultComponent());
    }
    public static FragmentDepartmentsBinding inflate(android.view.LayoutInflater inflater, android.databinding.DataBindingComponent bindingComponent) {
        return bind(inflater.inflate(com.zhydenko.vadim.esteldrive.R.layout.fragment_departments, null, false), bindingComponent);
    }
    public static FragmentDepartmentsBinding bind(android.view.View view) {
        return bind(view, android.databinding.DataBindingUtil.getDefaultComponent());
    }
    public static FragmentDepartmentsBinding bind(android.view.View view, android.databinding.DataBindingComponent bindingComponent) {
        if (!"layout/fragment_departments_0".equals(view.getTag())) {
            throw new RuntimeException("view tag isn't correct on view:" + view.getTag());
        }
        return new FragmentDepartmentsBinding(bindingComponent, view);
    }
    /* flag mapping
        flag 0 (0x1L): null
    flag mapping end*/
    //end
}