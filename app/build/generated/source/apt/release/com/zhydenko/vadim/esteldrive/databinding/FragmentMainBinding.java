package com.zhydenko.vadim.esteldrive.databinding;
import com.zhydenko.vadim.esteldrive.R;
import com.zhydenko.vadim.esteldrive.BR;
import android.view.View;
public class FragmentMainBinding extends android.databinding.ViewDataBinding  {

    private static final android.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.loadIndicator, 4);
        sViewsWithIds.put(R.id.cardCheckAuto, 5);
        sViewsWithIds.put(R.id.tAutoCheckConfig, 6);
        sViewsWithIds.put(R.id.tCheckTime, 7);
        sViewsWithIds.put(R.id.bScheduleConfig, 8);
        sViewsWithIds.put(R.id.cardCheckManual, 9);
        sViewsWithIds.put(R.id.tManualCheckConfig, 10);
        sViewsWithIds.put(R.id.tLastTimeCheck, 11);
        sViewsWithIds.put(R.id.bManualStart, 12);
        sViewsWithIds.put(R.id.cardAccess, 13);
        sViewsWithIds.put(R.id.tAccessConfig, 14);
        sViewsWithIds.put(R.id.rAccessList, 15);
        sViewsWithIds.put(R.id.bAccessConfig, 16);
        sViewsWithIds.put(R.id.cardNetwork, 17);
        sViewsWithIds.put(R.id.textView11, 18);
        sViewsWithIds.put(R.id.textView10, 19);
        sViewsWithIds.put(R.id.textView8, 20);
        sViewsWithIds.put(R.id.textView7, 21);
        sViewsWithIds.put(R.id.textView5, 22);
        sViewsWithIds.put(R.id.switchAutoUpdate, 23);
        sViewsWithIds.put(R.id.switchNetwork, 24);
        sViewsWithIds.put(R.id.textView, 25);
    }
    // views
    public final android.widget.TextView appVersion;
    public final android.widget.Button bAccessConfig;
    public final android.widget.Button bConfigCheckTime;
    public final android.widget.Button bManualStart;
    public final com.zhydenko.vadim.esteldrive.custom_view.CircularProgressButton bScheduleConfig;
    public final android.support.v7.widget.CardView cardAccess;
    public final android.support.v7.widget.CardView cardCheckAuto;
    public final android.support.v7.widget.CardView cardCheckManual;
    public final android.support.v7.widget.CardView cardNetwork;
    public final android.widget.TextView lastTimeCheck;
    public final android.widget.ProgressBar loadIndicator;
    private final android.widget.ScrollView mboundView0;
    public final android.support.v7.widget.RecyclerView rAccessList;
    public final android.widget.Switch switchAutoUpdate;
    public final android.widget.Switch switchNetwork;
    public final android.widget.TextView tAccessConfig;
    public final android.widget.TextView tAutoCheckConfig;
    public final android.widget.TextView tCheckTime;
    public final android.widget.TextView tLastTimeCheck;
    public final android.widget.TextView tManualCheckConfig;
    public final android.widget.TextView textView;
    public final android.widget.TextView textView10;
    public final android.widget.TextView textView11;
    public final android.widget.TextView textView5;
    public final android.widget.TextView textView7;
    public final android.widget.TextView textView8;
    // variables
    private com.zhydenko.vadim.esteldrive.models.User mModel;
    // values
    // listeners
    // Inverse Binding Event Handlers

    public FragmentMainBinding(android.databinding.DataBindingComponent bindingComponent, View root) {
        super(bindingComponent, root, 0);
        final Object[] bindings = mapBindings(bindingComponent, root, 26, sIncludes, sViewsWithIds);
        this.appVersion = (android.widget.TextView) bindings[3];
        this.appVersion.setTag(null);
        this.bAccessConfig = (android.widget.Button) bindings[16];
        this.bConfigCheckTime = (android.widget.Button) bindings[1];
        this.bConfigCheckTime.setTag(null);
        this.bManualStart = (android.widget.Button) bindings[12];
        this.bScheduleConfig = (com.zhydenko.vadim.esteldrive.custom_view.CircularProgressButton) bindings[8];
        this.cardAccess = (android.support.v7.widget.CardView) bindings[13];
        this.cardCheckAuto = (android.support.v7.widget.CardView) bindings[5];
        this.cardCheckManual = (android.support.v7.widget.CardView) bindings[9];
        this.cardNetwork = (android.support.v7.widget.CardView) bindings[17];
        this.lastTimeCheck = (android.widget.TextView) bindings[2];
        this.lastTimeCheck.setTag(null);
        this.loadIndicator = (android.widget.ProgressBar) bindings[4];
        this.mboundView0 = (android.widget.ScrollView) bindings[0];
        this.mboundView0.setTag(null);
        this.rAccessList = (android.support.v7.widget.RecyclerView) bindings[15];
        this.switchAutoUpdate = (android.widget.Switch) bindings[23];
        this.switchNetwork = (android.widget.Switch) bindings[24];
        this.tAccessConfig = (android.widget.TextView) bindings[14];
        this.tAutoCheckConfig = (android.widget.TextView) bindings[6];
        this.tCheckTime = (android.widget.TextView) bindings[7];
        this.tLastTimeCheck = (android.widget.TextView) bindings[11];
        this.tManualCheckConfig = (android.widget.TextView) bindings[10];
        this.textView = (android.widget.TextView) bindings[25];
        this.textView10 = (android.widget.TextView) bindings[19];
        this.textView11 = (android.widget.TextView) bindings[18];
        this.textView5 = (android.widget.TextView) bindings[22];
        this.textView7 = (android.widget.TextView) bindings[21];
        this.textView8 = (android.widget.TextView) bindings[20];
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x2L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    public boolean setVariable(int variableId, Object variable) {
        switch(variableId) {
            case BR.model :
                setModel((com.zhydenko.vadim.esteldrive.models.User) variable);
                return true;
        }
        return false;
    }

    public void setModel(com.zhydenko.vadim.esteldrive.models.User Model) {
        this.mModel = Model;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.model);
        super.requestRebind();
    }
    public com.zhydenko.vadim.esteldrive.models.User getModel() {
        return mModel;
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        java.lang.String modelLastTimeCheck = null;
        com.zhydenko.vadim.esteldrive.models.User model = mModel;
        java.lang.String modelScheduledTime = null;
        java.lang.String modelVersion = null;

        if ((dirtyFlags & 0x3L) != 0) {



                if (model != null) {
                    // read model.lastTimeCheck
                    modelLastTimeCheck = model.getLastTimeCheck();
                    // read model.scheduledTime
                    modelScheduledTime = model.getScheduledTime();
                    // read model.version
                    modelVersion = model.getVersion();
                }
        }
        // batch finished
        if ((dirtyFlags & 0x3L) != 0) {
            // api target 1

            android.databinding.adapters.TextViewBindingAdapter.setText(this.appVersion, modelVersion);
            android.databinding.adapters.TextViewBindingAdapter.setText(this.bConfigCheckTime, modelScheduledTime);
            android.databinding.adapters.TextViewBindingAdapter.setText(this.lastTimeCheck, modelLastTimeCheck);
        }
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;

    public static FragmentMainBinding inflate(android.view.LayoutInflater inflater, android.view.ViewGroup root, boolean attachToRoot) {
        return inflate(inflater, root, attachToRoot, android.databinding.DataBindingUtil.getDefaultComponent());
    }
    public static FragmentMainBinding inflate(android.view.LayoutInflater inflater, android.view.ViewGroup root, boolean attachToRoot, android.databinding.DataBindingComponent bindingComponent) {
        return android.databinding.DataBindingUtil.<FragmentMainBinding>inflate(inflater, com.zhydenko.vadim.esteldrive.R.layout.fragment_main, root, attachToRoot, bindingComponent);
    }
    public static FragmentMainBinding inflate(android.view.LayoutInflater inflater) {
        return inflate(inflater, android.databinding.DataBindingUtil.getDefaultComponent());
    }
    public static FragmentMainBinding inflate(android.view.LayoutInflater inflater, android.databinding.DataBindingComponent bindingComponent) {
        return bind(inflater.inflate(com.zhydenko.vadim.esteldrive.R.layout.fragment_main, null, false), bindingComponent);
    }
    public static FragmentMainBinding bind(android.view.View view) {
        return bind(view, android.databinding.DataBindingUtil.getDefaultComponent());
    }
    public static FragmentMainBinding bind(android.view.View view, android.databinding.DataBindingComponent bindingComponent) {
        if (!"layout/fragment_main_0".equals(view.getTag())) {
            throw new RuntimeException("view tag isn't correct on view:" + view.getTag());
        }
        return new FragmentMainBinding(bindingComponent, view);
    }
    /* flag mapping
        flag 0 (0x1L): model
        flag 1 (0x2L): null
    flag mapping end*/
    //end
}