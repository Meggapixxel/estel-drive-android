
package android.databinding;
import com.zhydenko.vadim.esteldrive.BR;
class DataBinderMapper  {
    final static int TARGET_MIN_SDK = 18;
    public DataBinderMapper() {
    }
    public android.databinding.ViewDataBinding getDataBinder(android.databinding.DataBindingComponent bindingComponent, android.view.View view, int layoutId) {
        switch(layoutId) {
                case com.zhydenko.vadim.esteldrive.R.layout.fragment_file:
                    return com.zhydenko.vadim.esteldrive.databinding.FragmentFileBinding.bind(view, bindingComponent);
                case com.zhydenko.vadim.esteldrive.R.layout.fragment_main:
                    return com.zhydenko.vadim.esteldrive.databinding.FragmentMainBinding.bind(view, bindingComponent);
                case com.zhydenko.vadim.esteldrive.R.layout.actvity_main:
                    return com.zhydenko.vadim.esteldrive.databinding.ActvityMainBinding.bind(view, bindingComponent);
                case com.zhydenko.vadim.esteldrive.R.layout.fragment_file_item:
                    return com.zhydenko.vadim.esteldrive.databinding.FragmentFileItemBinding.bind(view, bindingComponent);
                case com.zhydenko.vadim.esteldrive.R.layout.fragment_departments:
                    return com.zhydenko.vadim.esteldrive.databinding.FragmentDepartmentsBinding.bind(view, bindingComponent);
                case com.zhydenko.vadim.esteldrive.R.layout.department_list_item:
                    return com.zhydenko.vadim.esteldrive.databinding.DepartmentListItemBinding.bind(view, bindingComponent);
                case com.zhydenko.vadim.esteldrive.R.layout.fragment_logger_item:
                    return com.zhydenko.vadim.esteldrive.databinding.FragmentLoggerItemBinding.bind(view, bindingComponent);
                case com.zhydenko.vadim.esteldrive.R.layout.fragment_logger:
                    return com.zhydenko.vadim.esteldrive.databinding.FragmentLoggerBinding.bind(view, bindingComponent);
        }
        return null;
    }
    android.databinding.ViewDataBinding getDataBinder(android.databinding.DataBindingComponent bindingComponent, android.view.View[] views, int layoutId) {
        switch(layoutId) {
        }
        return null;
    }
    int getLayoutId(String tag) {
        if (tag == null) {
            return 0;
        }
        final int code = tag.hashCode();
        switch(code) {
            case -1178823145: {
                if(tag.equals("layout/fragment_file_0")) {
                    return com.zhydenko.vadim.esteldrive.R.layout.fragment_file;
                }
                break;
            }
            case -985887980: {
                if(tag.equals("layout/fragment_main_0")) {
                    return com.zhydenko.vadim.esteldrive.R.layout.fragment_main;
                }
                break;
            }
            case -473090428: {
                if(tag.equals("layout/actvity_main_0")) {
                    return com.zhydenko.vadim.esteldrive.R.layout.actvity_main;
                }
                break;
            }
            case -2117660547: {
                if(tag.equals("layout/fragment_file_item_0")) {
                    return com.zhydenko.vadim.esteldrive.R.layout.fragment_file_item;
                }
                break;
            }
            case -1200390232: {
                if(tag.equals("layout/fragment_departments_0")) {
                    return com.zhydenko.vadim.esteldrive.R.layout.fragment_departments;
                }
                break;
            }
            case 789412829: {
                if(tag.equals("layout/department_list_item_0")) {
                    return com.zhydenko.vadim.esteldrive.R.layout.department_list_item;
                }
                break;
            }
            case -1032301463: {
                if(tag.equals("layout/fragment_logger_item_0")) {
                    return com.zhydenko.vadim.esteldrive.R.layout.fragment_logger_item;
                }
                break;
            }
            case -516714837: {
                if(tag.equals("layout/fragment_logger_0")) {
                    return com.zhydenko.vadim.esteldrive.R.layout.fragment_logger;
                }
                break;
            }
        }
        return 0;
    }
    String convertBrIdToString(int id) {
        if (id < 0 || id >= InnerBrLookup.sKeys.length) {
            return null;
        }
        return InnerBrLookup.sKeys[id];
    }
    private static class InnerBrLookup {
        static String[] sKeys = new String[]{
            "_all"
            ,"content"
            ,"data"
            ,"fragment"
            ,"model"};
    }
}