package com.zhydenko.vadim.esteldrive.databinding;
import com.zhydenko.vadim.esteldrive.R;
import com.zhydenko.vadim.esteldrive.BR;
import android.view.View;
public class FragmentFileItemBinding extends android.databinding.ViewDataBinding  {

    private static final android.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.imageView, 4);
    }
    // views
    public final android.widget.TextView fileDate;
    public final android.widget.TextView fileName;
    public final android.widget.TextView fileSize;
    public final android.widget.ImageView imageView;
    private final android.support.v7.widget.CardView mboundView0;
    // variables
    private com.zhydenko.vadim.esteldrive.models.Content mContent;
    // values
    // listeners
    // Inverse Binding Event Handlers

    public FragmentFileItemBinding(android.databinding.DataBindingComponent bindingComponent, View root) {
        super(bindingComponent, root, 0);
        final Object[] bindings = mapBindings(bindingComponent, root, 5, sIncludes, sViewsWithIds);
        this.fileDate = (android.widget.TextView) bindings[2];
        this.fileDate.setTag(null);
        this.fileName = (android.widget.TextView) bindings[1];
        this.fileName.setTag(null);
        this.fileSize = (android.widget.TextView) bindings[3];
        this.fileSize.setTag(null);
        this.imageView = (android.widget.ImageView) bindings[4];
        this.mboundView0 = (android.support.v7.widget.CardView) bindings[0];
        this.mboundView0.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x2L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    public boolean setVariable(int variableId, Object variable) {
        switch(variableId) {
            case BR.content :
                setContent((com.zhydenko.vadim.esteldrive.models.Content) variable);
                return true;
        }
        return false;
    }

    public void setContent(com.zhydenko.vadim.esteldrive.models.Content Content) {
        this.mContent = Content;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.content);
        super.requestRebind();
    }
    public com.zhydenko.vadim.esteldrive.models.Content getContent() {
        return mContent;
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        java.lang.String contentConvertedSize = null;
        java.lang.String contentChangeDateJavaLangObjectNullStringValueOfContentChangeDateJavaLangObjectNull = null;
        java.lang.String contentName = null;
        com.zhydenko.vadim.esteldrive.models.Content content = mContent;
        java.util.Date contentChangeDate = null;
        boolean contentChangeDateJavaLangObjectNull = false;
        java.lang.String stringValueOfContentChangeDate = null;

        if ((dirtyFlags & 0x3L) != 0) {



                if (content != null) {
                    // read content.convertedSize
                    contentConvertedSize = content.getConvertedSize();
                    // read content.name
                    contentName = content.getName();
                    // read content.changeDate
                    contentChangeDate = content.getChangeDate();
                }


                // read content.changeDate != null
                contentChangeDateJavaLangObjectNull = (contentChangeDate) != (null);
            if((dirtyFlags & 0x3L) != 0) {
                if(contentChangeDateJavaLangObjectNull) {
                        dirtyFlags |= 0x8L;
                }
                else {
                        dirtyFlags |= 0x4L;
                }
            }
        }
        // batch finished

        if ((dirtyFlags & 0x8L) != 0) {

                // read String.valueOf(content.changeDate)
                stringValueOfContentChangeDate = java.lang.String.valueOf(contentChangeDate);
        }

        if ((dirtyFlags & 0x3L) != 0) {

                // read content.changeDate != null ? String.valueOf(content.changeDate) : null
                contentChangeDateJavaLangObjectNullStringValueOfContentChangeDateJavaLangObjectNull = ((contentChangeDateJavaLangObjectNull) ? (stringValueOfContentChangeDate) : (null));
        }
        // batch finished
        if ((dirtyFlags & 0x3L) != 0) {
            // api target 1

            android.databinding.adapters.TextViewBindingAdapter.setText(this.fileDate, contentChangeDateJavaLangObjectNullStringValueOfContentChangeDateJavaLangObjectNull);
            android.databinding.adapters.TextViewBindingAdapter.setText(this.fileName, contentName);
            android.databinding.adapters.TextViewBindingAdapter.setText(this.fileSize, contentConvertedSize);
        }
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;

    public static FragmentFileItemBinding inflate(android.view.LayoutInflater inflater, android.view.ViewGroup root, boolean attachToRoot) {
        return inflate(inflater, root, attachToRoot, android.databinding.DataBindingUtil.getDefaultComponent());
    }
    public static FragmentFileItemBinding inflate(android.view.LayoutInflater inflater, android.view.ViewGroup root, boolean attachToRoot, android.databinding.DataBindingComponent bindingComponent) {
        return android.databinding.DataBindingUtil.<FragmentFileItemBinding>inflate(inflater, com.zhydenko.vadim.esteldrive.R.layout.fragment_file_item, root, attachToRoot, bindingComponent);
    }
    public static FragmentFileItemBinding inflate(android.view.LayoutInflater inflater) {
        return inflate(inflater, android.databinding.DataBindingUtil.getDefaultComponent());
    }
    public static FragmentFileItemBinding inflate(android.view.LayoutInflater inflater, android.databinding.DataBindingComponent bindingComponent) {
        return bind(inflater.inflate(com.zhydenko.vadim.esteldrive.R.layout.fragment_file_item, null, false), bindingComponent);
    }
    public static FragmentFileItemBinding bind(android.view.View view) {
        return bind(view, android.databinding.DataBindingUtil.getDefaultComponent());
    }
    public static FragmentFileItemBinding bind(android.view.View view, android.databinding.DataBindingComponent bindingComponent) {
        if (!"layout/fragment_file_item_0".equals(view.getTag())) {
            throw new RuntimeException("view tag isn't correct on view:" + view.getTag());
        }
        return new FragmentFileItemBinding(bindingComponent, view);
    }
    /* flag mapping
        flag 0 (0x1L): content
        flag 1 (0x2L): null
        flag 2 (0x3L): content.changeDate != null ? String.valueOf(content.changeDate) : null
        flag 3 (0x4L): content.changeDate != null ? String.valueOf(content.changeDate) : null
    flag mapping end*/
    //end
}