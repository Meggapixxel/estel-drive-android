package com.zhydenko.vadim.esteldrive.databinding;
import com.zhydenko.vadim.esteldrive.R;
import com.zhydenko.vadim.esteldrive.BR;
import android.view.View;
public class FragmentLoggerItemBinding extends android.databinding.ViewDataBinding  {

    private static final android.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = null;
    }
    // views
    private final android.support.constraint.ConstraintLayout mboundView0;
    public final android.widget.TextView texto1;
    public final android.widget.TextView texto2;
    public final android.widget.TextView texto3;
    // variables
    private com.zhydenko.vadim.esteldrive.models.LoggerItem mData;
    // values
    // listeners
    // Inverse Binding Event Handlers

    public FragmentLoggerItemBinding(android.databinding.DataBindingComponent bindingComponent, View root) {
        super(bindingComponent, root, 0);
        final Object[] bindings = mapBindings(bindingComponent, root, 4, sIncludes, sViewsWithIds);
        this.mboundView0 = (android.support.constraint.ConstraintLayout) bindings[0];
        this.mboundView0.setTag(null);
        this.texto1 = (android.widget.TextView) bindings[1];
        this.texto1.setTag(null);
        this.texto2 = (android.widget.TextView) bindings[2];
        this.texto2.setTag(null);
        this.texto3 = (android.widget.TextView) bindings[3];
        this.texto3.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x2L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    public boolean setVariable(int variableId, Object variable) {
        switch(variableId) {
            case BR.data :
                setData((com.zhydenko.vadim.esteldrive.models.LoggerItem) variable);
                return true;
        }
        return false;
    }

    public void setData(com.zhydenko.vadim.esteldrive.models.LoggerItem Data) {
        this.mData = Data;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.data);
        super.requestRebind();
    }
    public com.zhydenko.vadim.esteldrive.models.LoggerItem getData() {
        return mData;
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        java.lang.String dataName = null;
        boolean dataStatusEqualsJavaLangString0 = false;
        com.zhydenko.vadim.esteldrive.models.LoggerItem data = mData;
        java.lang.String dataDate = null;
        int dataStatusEqualsJavaLangString0Texto2AndroidColorMaterialGreenTexto2AndroidColorMaterialRed = 0;
        java.lang.String dataStatus = null;
        java.lang.String dataStatusEqualsJavaLangString0Texto2AndroidStringOpenTexto2AndroidStringError = null;

        if ((dirtyFlags & 0x3L) != 0) {



                if (data != null) {
                    // read data.name
                    dataName = data.getName();
                    // read data.date
                    dataDate = data.getDate();
                    // read data.status
                    dataStatus = data.getStatus();
                }


                if (dataStatus != null) {
                    // read data.status.equals("0")
                    dataStatusEqualsJavaLangString0 = dataStatus.equals("0");
                }
            if((dirtyFlags & 0x3L) != 0) {
                if(dataStatusEqualsJavaLangString0) {
                        dirtyFlags |= 0x8L;
                        dirtyFlags |= 0x20L;
                }
                else {
                        dirtyFlags |= 0x4L;
                        dirtyFlags |= 0x10L;
                }
            }


                // read data.status.equals("0") ? @android:color/material_green : @android:color/material_red
                dataStatusEqualsJavaLangString0Texto2AndroidColorMaterialGreenTexto2AndroidColorMaterialRed = ((dataStatusEqualsJavaLangString0) ? (getColorFromResource(texto2, R.color.material_green)) : (getColorFromResource(texto2, R.color.material_red)));
                // read data.status.equals("0") ? @android:string/open : @android:string/error
                dataStatusEqualsJavaLangString0Texto2AndroidStringOpenTexto2AndroidStringError = ((dataStatusEqualsJavaLangString0) ? (texto2.getResources().getString(R.string.open)) : (texto2.getResources().getString(R.string.error)));
        }
        // batch finished
        if ((dirtyFlags & 0x3L) != 0) {
            // api target 1

            android.databinding.adapters.TextViewBindingAdapter.setText(this.texto1, dataName);
            android.databinding.adapters.TextViewBindingAdapter.setText(this.texto2, dataStatusEqualsJavaLangString0Texto2AndroidStringOpenTexto2AndroidStringError);
            this.texto2.setTextColor(dataStatusEqualsJavaLangString0Texto2AndroidColorMaterialGreenTexto2AndroidColorMaterialRed);
            android.databinding.adapters.TextViewBindingAdapter.setText(this.texto3, dataDate);
        }
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;

    public static FragmentLoggerItemBinding inflate(android.view.LayoutInflater inflater, android.view.ViewGroup root, boolean attachToRoot) {
        return inflate(inflater, root, attachToRoot, android.databinding.DataBindingUtil.getDefaultComponent());
    }
    public static FragmentLoggerItemBinding inflate(android.view.LayoutInflater inflater, android.view.ViewGroup root, boolean attachToRoot, android.databinding.DataBindingComponent bindingComponent) {
        return android.databinding.DataBindingUtil.<FragmentLoggerItemBinding>inflate(inflater, com.zhydenko.vadim.esteldrive.R.layout.fragment_logger_item, root, attachToRoot, bindingComponent);
    }
    public static FragmentLoggerItemBinding inflate(android.view.LayoutInflater inflater) {
        return inflate(inflater, android.databinding.DataBindingUtil.getDefaultComponent());
    }
    public static FragmentLoggerItemBinding inflate(android.view.LayoutInflater inflater, android.databinding.DataBindingComponent bindingComponent) {
        return bind(inflater.inflate(com.zhydenko.vadim.esteldrive.R.layout.fragment_logger_item, null, false), bindingComponent);
    }
    public static FragmentLoggerItemBinding bind(android.view.View view) {
        return bind(view, android.databinding.DataBindingUtil.getDefaultComponent());
    }
    public static FragmentLoggerItemBinding bind(android.view.View view, android.databinding.DataBindingComponent bindingComponent) {
        if (!"layout/fragment_logger_item_0".equals(view.getTag())) {
            throw new RuntimeException("view tag isn't correct on view:" + view.getTag());
        }
        return new FragmentLoggerItemBinding(bindingComponent, view);
    }
    /* flag mapping
        flag 0 (0x1L): data
        flag 1 (0x2L): null
        flag 2 (0x3L): data.status.equals("0") ? @android:color/material_green : @android:color/material_red
        flag 3 (0x4L): data.status.equals("0") ? @android:color/material_green : @android:color/material_red
        flag 4 (0x5L): data.status.equals("0") ? @android:string/open : @android:string/error
        flag 5 (0x6L): data.status.equals("0") ? @android:string/open : @android:string/error
    flag mapping end*/
    //end
}