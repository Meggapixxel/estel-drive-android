/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.zhydenko.vadim.esteldrive;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String APPLICATION_ID = "com.zhydenko.vadim.esteldrive";
  public static final String BUILD_TYPE = "debug";
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = 2;
  public static final String VERSION_NAME = "2.0.0 beta 1 (3)";
}
